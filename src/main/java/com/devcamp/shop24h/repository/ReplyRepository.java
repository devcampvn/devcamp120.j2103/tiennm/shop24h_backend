package com.devcamp.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.Reply;
import com.devcamp.shop24h.output.UserReply;

public interface ReplyRepository extends JpaRepository<Reply, Long> {
	
	/**
	 * Hiển thị reply theo reviewId
	 * @Param reviewId
	 */
	@Query(value = "SELECT * FROM reply WHERE review_id = :reviewId" , nativeQuery = true)
	Optional<Reply> getReplyByReviewId(@Param("reviewId") Long reviewId);
	
	
	/**
	 * Hiển thị reply theo reviewId
	 * @Param reviewId
	 */
	@Query(value = "SELECT rw.id AS reviewId , u.username AS userName , r.reply AS reply , r.create_date AS createDate FROM reply r JOIN users u ON u.id = r.user_id JOIN review rw ON rw.id = r.review_id WHERE r.review_id = :reviewId" , nativeQuery = true)
	Optional<UserReply> findReplyByReviewId(@Param("reviewId") Long reviewId);
}
