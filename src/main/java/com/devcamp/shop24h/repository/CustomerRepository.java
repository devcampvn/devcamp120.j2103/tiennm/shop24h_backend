package com.devcamp.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.Customer;
import com.devcamp.shop24h.output.ListOrder;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
	
	/**
	 * Tìm kiếm khách hàng theo số điện thoại
	 * @Param phoneNumber
	 */
	@Query(value="SELECT * FROM customer WHERE phone_number = :phoneNumber" , nativeQuery = true)
	Optional<Customer> findByPhoneNumber(@Param("phoneNumber") String phoneNumber);
	
	
	/**
	 * Tìm kiếm khách hàng theo email
	 * @Param email
	 */
	@Query(value = "SELECT * FROM customer WHERE email = :email " , nativeQuery = true)
	Optional<Customer> findCustomerByEmail(@Param("email") String email);
	
	/**
	 * Tìm kiếm order của khách hàng theo email
	 * @Param email
	 */
	@Query(value = "SELECT p.id AS productId , p.product_name AS productName , p.product_code AS productCode , o.order_date AS orderDate , od.quantity_order AS quanlity , od.price_each AS price FROM customer c JOIN orders o ON o.customer_id = c.id JOIN order_details od ON od.order_id = o.id JOIN product p ON p.id = od.product_id WHERE c.email = :email" , nativeQuery = true)
	List<ListOrder> getAllOrdersUser(@Param("email") String email);
	
	
	/**
	 * Tìm kiếm khách hàng theo email
	 * @Param email
	 */
	Optional<Customer> findByEmail(String email);
}
