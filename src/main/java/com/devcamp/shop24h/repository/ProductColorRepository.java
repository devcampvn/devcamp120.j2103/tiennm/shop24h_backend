package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.ProductColor;

public interface ProductColorRepository extends JpaRepository<ProductColor, Integer> {

}
