package com.devcamp.shop24h.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.Product;
import com.devcamp.shop24h.output.ProductHandle;
import com.devcamp.shop24h.output.RelateProduct;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	/**
	 * Đếm số sản phẩm
	 */
	@Query(value="SELECT p.product_line_id AS productLineId, pl.image_product_line AS urlImage, pl.product_line AS productLine , COUNT(p.id) AS productCount  FROM product p JOIN product_line pl ON pl.id = p.product_line_id GROUP BY pl.product_line" , nativeQuery = true)
	List<ProductHandle> getProductCount();
	
	/**
	 * Hiển thị 8 sản phẩm theo id giảm dần
	 */
	@Query(value = "SELECT * FROM product ORDER BY id DESC LIMIT 8" , nativeQuery = true)
	List<Product> getAllProductDESC();
	
	/**
	 * Hiển thị sản phẩm của giày nam
	 */
	@Query(value = "SELECT * FROM product p JOIN product_line pl ON pl.id = p.product_line_id WHERE pl.product_line LIKE '%nam%' ORDER BY p.id DESC LIMIT 8" , nativeQuery = true)
	List<Product> getAllManShoes();
	
	/**
     * Hiển thị sản phẩm của giày nữ	
	 */
	@Query(value = "SELECT * FROM product p JOIN product_line pl ON pl.id = p.product_line_id WHERE pl.product_line LIKE '%nữ%' ORDER BY p.id DESC LIMIT 8" , nativeQuery = true)
	List<Product> getAllWomanShoes();
	
	/**
     * Tìm kiếm sản phẩm theo product color id
     * @Param id	
	 */
	@Query(value="SELECT * FROM product WHERE product_color_id = :id" , nativeQuery = true)
	List<Product> findProductByProductColorId(@Param("id") int id);
	
	/**
     * Tìm kiếm sản phẩm theo product brand id	
     * @Param id
	 */
	@Query(value="SELECT * FROM product WHERE product_brand_id = :id" , nativeQuery = true)
	List<Product> findProductByProductBrandId(@Param("id") int id);
	
	/**
     * Tìm kiếm sản phẩm theo product line id	
     * @Param id
	 */
	@Query(value="SELECT * FROM product WHERE product_line_id = :id" , nativeQuery = true)
	List<Product> findProductByProductLineId(@Param("id") int id);
	
	/**
     * Hiển thị sản phẩm liên quan	
     * @Param startRow
	 */
	@Query(value="SELECT * FROM product LIMIT 6 OFFSET :startRow" , nativeQuery = true)
	List<Product> sortProductByNumber(@Param("startRow") int startRow);
	
	/**
     * Hiển thị danh sách sản phẩm có phân trang
     * Pageable pageable
	 */
	@Query(value = "SELECT * FROM product " , nativeQuery = true)
	Page<Product> getAllProductPageable(Pageable pageable);
	
	/**
     * Tìm kiếm sản phẩm theo product line id
     * @Param productLineId
     * @Param productId
	 */
	@Query(value="SELECT * FROM product WHERE product_line_id = :productLineId AND id != :productId LIMIT 6" , nativeQuery = true)
	List<Product> findProductByProductLineIdOffsetProductId(@Param("productLineId") int productLineId , @Param("productId") int productId);
	
	/**
     * Tìm kiếm sản phẩm theo list product id
     * @Param ids
	 */
	@Query(value="SELECT * FROM product WHERE id in :ids", nativeQuery = true)
	List<Product> findProducttByIds(@Param("ids") List<Long> ids);
	
	/**
     * Tìm kiếm sản phẩm theo giá
     * @Param minPrice
     * @Param maxPrice
     * Pageable pageable
	 */
	@Query(value="SELECT * FROM product WHERE price BETWEEN :minPrice AND :maxPrice" , nativeQuery = true)
	Page<Product> findProductByPrice(@Param("minPrice") BigDecimal minPrice , @Param("maxPrice") BigDecimal maxPrice , Pageable pageable);
	
	
	/**
     * Tìm kiếm sản phẩm theo product line id vs product color id
     * @Param productLineIds
     * @Param productBrandIds
	 */
	@Query(value="SELECT * FROM product WHERE (( product_line_id IN (:productLineIds) OR COALESCE (:productLineIds) IS NUll ) AND ( product_brand_id IN (:productBrandIds) OR COALESCE (:productBrandIds) IS NUll ) AND (product_color_id IN (:productColorIds) OR COALESCE (:productColorIds) IS NUll))" ,nativeQuery = true)
	Page<Product> findProductByProductLineAndBrandAndColorId(@Param("productLineIds") List<Long> productLineIds , @Param("productBrandIds") List<Long> productBrandIds , @Param("productColorIds") List<Long> productColorIds , Pageable pageable);

}
