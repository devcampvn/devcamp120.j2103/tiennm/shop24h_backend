package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.OrderDetail;
import com.devcamp.shop24h.output.FilterCustomer;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {
	
	/**
	 * Tìm kiến orderDetail theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT * FROM order_details  WHERE product_id = :productId" , nativeQuery = true)
	List<OrderDetail> findOrderDetailByProductId(@Param("@Param") int productId);
	
	/**
	 * Tìm kiến orderDetail theo orderId
	 * @Param id
	 */
	List<OrderDetail> findByOrderId(Integer id);
	
	/**
	 * Tìm kiến khách hàng loại vip
	 */
	@Query(value = "SELECT CONCAT(c.first_name , \" \" , c.last_name) AS customer , SUM(od.price_each) AS total   FROM order_details od JOIN orders o ON o.id = od.order_id LEFT JOIN customer c ON c.id = o.customer_id GROUP BY od.order_id HAVING SUM(od.price_each) > '5000000' AND SUM(od.price_each) < '10000000'" , nativeQuery = true)
	List<FilterCustomer> findCustomerVip();
	
	
	/**
	 * Tìm kiến khách hàng loại silver
	 */
	@Query(value = "SELECT CONCAT(c.first_name , \" \" , c.last_name) AS customer , SUM(od.price_each) AS total   FROM order_details od JOIN orders o ON o.id = od.order_id LEFT JOIN customer c ON c.id = o.customer_id GROUP BY od.order_id HAVING SUM(od.price_each) > '10000000' AND SUM(od.price_each) < '20000000'" , nativeQuery = true)
	List<FilterCustomer> findCustomerSilver();
	
	
	/**
	 * Tìm kiến khách hàng loại gold
	 */
	@Query(value = "SELECT CONCAT(c.first_name , \" \" , c.last_name) AS customer , SUM(od.price_each) AS total   FROM order_details od JOIN orders o ON o.id = od.order_id LEFT JOIN customer c ON c.id = o.customer_id GROUP BY od.order_id HAVING SUM(od.price_each) > '20000000' AND SUM(od.price_each) < '50000000'" , nativeQuery = true)
	List<FilterCustomer> findCustomerGold();
	
	
	/**
	 * Tìm kiến khách hàng loại diamond
	 */
	@Query(value = "SELECT CONCAT(c.first_name , \" \" , c.last_name) AS customer , SUM(od.price_each) AS total   FROM order_details od JOIN orders o ON o.id = od.order_id LEFT JOIN customer c ON c.id = o.customer_id GROUP BY od.order_id HAVING SUM(od.price_each) > '50000000' " , nativeQuery = true)
	List<FilterCustomer> findCustomerDiamond();
	
	
}
