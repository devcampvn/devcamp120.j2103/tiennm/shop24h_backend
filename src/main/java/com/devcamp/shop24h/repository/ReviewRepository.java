package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.Review;
import com.devcamp.shop24h.output.UserReview;

public interface ReviewRepository extends JpaRepository<Review, Long> {
	
	/**
	 * Hiển thị list review theo id giảm dần
	 */
	@Query(value = "SELECT * FROM review ORDER BY id DESC;" , nativeQuery = true)
	List<Review> findAllReviewByIdDesc();
	
	/**
	 * Đếm số lượt rating 5 sao theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT COUNT(rating) FROM review WHERE rating = 5 AND product_id = :productId", nativeQuery = true)
	Long countRatingByFive(@Param("productId") int productId);
	
	/**
	 * Đếm số lượt rating 4 sao theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT COUNT(rating) FROM review WHERE rating = 4 AND product_id = :productId", nativeQuery = true)
	Long countRatingByFour(@Param("productId") int productId);
	
	/**
	 * Đếm số lượt rating 3 sao theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT COUNT(rating) FROM review WHERE rating = 3 AND product_id = :productId", nativeQuery = true)
	Long countRatingByThree(@Param("productId") int productId);
	
	/**
	 * Đếm số lượt rating 2 sao theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT COUNT(rating) FROM review WHERE rating = 2 AND product_id = :productId", nativeQuery = true)
	Long countRatingByTwo(@Param("productId") int productId);
	
	/**
	 * Đếm số lượt rating 1 sao theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT COUNT(rating) FROM review WHERE rating = 1 AND product_id = :productId", nativeQuery = true)
	Long countRatingByOne(@Param("productId") int productId);
	
	/**
	 * Đếm số lượt rating theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT COUNT(rating) FROM review WHERE product_id = :productId", nativeQuery = true)
	Long countRating(@Param("productId") int productId);
	
	/**
	 * Hiển thị danh sách review theo product id
	 * @Param productId
	 */
	@Query(value = "SELECT r.id AS reviewId , u.username AS userName , r.rating AS rating , r.review AS review , r.create_date AS createDate , rp.reply AS reply , rp.create_date AS replyDate FROM review r JOIN users u ON u.id = r.user_id LEFT JOIN reply rp ON rp.review_id = r.id WHERE r.product_id = :productId " , nativeQuery = true)
	List<UserReview> getAllReviewByProductId(@Param("productId") int productId);
}
