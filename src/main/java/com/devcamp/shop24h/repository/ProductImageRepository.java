package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.ProductImages;

public interface ProductImageRepository extends JpaRepository<ProductImages, Integer> {
	List<ProductImages> findByProductId(Integer id);
}
