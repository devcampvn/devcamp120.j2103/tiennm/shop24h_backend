package com.devcamp.shop24h.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.output.UserRole;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	/**
	 * Tìm kiếm user theo email
	 */
	Optional<User> findByEmail(String email);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);
	
	/**
	 * Tìm kiếm user theo userName
	 * @Param username
	 */
	@Query(value = "SELECT * FROM users WHERE username = :username" , nativeQuery = true)
	Optional<User> findByUserName(@Param("username") String username);
	
	
	/**
	 * Tìm kiếm list user
	 */
	@Query(value = "SELECT u.id AS userId , u.username AS userName , u.email AS email, u.password AS password , r.name AS roleName FROM users u  JOIN user_roles ur ON ur.user_id = u.id  JOIN roles r ON r.id = ur.role_id " , nativeQuery = true)
	List<UserRole> findAllUser();
}
