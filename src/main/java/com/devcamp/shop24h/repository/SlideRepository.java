package com.devcamp.shop24h.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shop24h.model.Slide;

public interface SlideRepository extends JpaRepository<Slide, Integer> {

}
