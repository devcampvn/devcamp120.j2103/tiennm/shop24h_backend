package com.devcamp.shop24h.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.shop24h.model.Orders;
import com.devcamp.shop24h.output.CountOrderByDate;
import com.devcamp.shop24h.output.CountOrderByWeek;

public interface OrderRepository extends JpaRepository<Orders, Integer> {
	
	/**
	 * Tìm kiếm order theo customer Id  
	 * @Param id
	 */
	@Query(value="SELECT * FROM orders JOIN customer ON customer.id = orders.customer_id WHERE customer.id = :id" , nativeQuery = true)
	List<Orders> findOrdersByCustomerId(@Param("id") int id);
	
	/**
	 * Tổng số Order  
	 */
	@Query(value="SELECT SUM(od.price_each) AS total , DATE(order_date) AS orderDate " +
	             "FROM orders o " + 
			     "JOIN order_details od ON od.order_id = o.id " + 
	             "GROUP BY DATE(order_date)", nativeQuery = true)
	List<CountOrderByDate> getCountOrder();
	
	/**
	 * Tổng số Order theo tuần
	 */
	@Query(value="SELECT SUM(od.price_each) AS total , FROM_DAYS(TO_DAYS(o.order_date) -MOD(TO_DAYS(o.order_date) -1, 7)) AS orderWeek " +
            "FROM orders o " + 
		    "JOIN order_details od ON od.order_id = o.id " + 
            "GROUP BY FROM_DAYS(TO_DAYS(o.order_date) -MOD(TO_DAYS(o.order_date) -1, 7))", nativeQuery = true)
	List<CountOrderByWeek> getCountOrderByWeek();
}
