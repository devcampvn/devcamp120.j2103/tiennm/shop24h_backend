package com.devcamp.shop24h.output;

import java.math.BigDecimal;
import java.util.Date;

public interface ListOrder {
	
	public int getProductId();
	public String getProductName();
	public String getProductCode();
	public Date getOrderDate();
	public int getQuanlity();
	public BigDecimal getPrice();
}
