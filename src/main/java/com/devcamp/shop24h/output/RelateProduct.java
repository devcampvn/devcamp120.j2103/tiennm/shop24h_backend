package com.devcamp.shop24h.output;

import java.math.BigDecimal;

public interface RelateProduct {
	
	public int getProductId();
	public String getProductName();
	public String getImgUrl();
	public BigDecimal getProductPrice();
}
