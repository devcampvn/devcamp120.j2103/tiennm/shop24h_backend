package com.devcamp.shop24h.output;

import java.util.List;

public class FilterProductList {
		
	private List<Long> productLineIds;
	private List<Long> productBrandIds;
	private List<Long> productColorIds;
	public FilterProductList() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FilterProductList(List<Long> productLineIds, List<Long> productBrandIds, List<Long> productColorIds) {
		super();
		this.productLineIds = productLineIds;
		this.productBrandIds = productBrandIds;
		this.productColorIds = productColorIds;
	}
	public List<Long> getProductLineIds() {
		return productLineIds;
	}
	public void setProductLineIds(List<Long> productLineIds) {
		this.productLineIds = productLineIds;
	}
	public List<Long> getProductBrandIds() {
		return productBrandIds;
	}
	public void setProductBrandIds(List<Long> productBrandIds) {
		this.productBrandIds = productBrandIds;
	}
	public List<Long> getProductColorIds() {
		return productColorIds;
	}
	public void setProductColorIds(List<Long> productColorIds) {
		this.productColorIds = productColorIds;
	}
	
}
