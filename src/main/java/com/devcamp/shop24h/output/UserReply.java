package com.devcamp.shop24h.output;

import java.util.Date;

public interface UserReply {
	
	public Long getReviewId();
	public String getUserName();
	public String getReply();
	public Date getCreateDate();
}
