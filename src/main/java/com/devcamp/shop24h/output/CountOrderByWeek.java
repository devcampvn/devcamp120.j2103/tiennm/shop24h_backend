package com.devcamp.shop24h.output;

import java.util.Date;

public interface CountOrderByWeek {
	public int getTotal();
	public Date getOrderWeek();
}
