package com.devcamp.shop24h.output;


public interface ProductHandle {
	
	public int getProductLineId();
	public String[] getUrlImage();
	public String getProductLine();
	public int getProductCount();
	
}
