package com.devcamp.shop24h.output;

import java.math.BigDecimal;

public class RequestProductPriceList {
	private BigDecimal minPrice;
	private BigDecimal maxPrice;
	public RequestProductPriceList() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RequestProductPriceList(BigDecimal minPrice, BigDecimal maxPrice) {
		super();
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}
	public BigDecimal getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(BigDecimal minPrice) {
		this.minPrice = minPrice;
	}
	public BigDecimal getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(BigDecimal maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	
}
