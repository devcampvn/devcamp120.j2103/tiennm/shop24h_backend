package com.devcamp.shop24h.output;

import java.util.Date;

public interface UserReview {
	
	public Long getReviewId();
	public String getUserName();
	public String getReview();
	public Integer getRating();
	public Date getCreateDate();
	public String getReply();
	public Date getReplyDate();
	
}
