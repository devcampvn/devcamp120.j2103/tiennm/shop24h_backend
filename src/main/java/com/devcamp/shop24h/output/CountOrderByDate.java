package com.devcamp.shop24h.output;

import java.util.Date;

public interface CountOrderByDate {
	public int getTotal();
	public Date getOrderDate();
}
