package com.devcamp.shop24h.output;

public interface UserRole {
	
	public Long getUserId();
	public String getUserName();
	public String getEmail();
	public String getPassword();
	public String getRoleName();
}
