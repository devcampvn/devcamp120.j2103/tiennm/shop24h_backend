package com.devcamp.shop24h.model;

public enum ERole {
	ROLE_USER,
    ROLE_MANAGER,
    ROLE_ADMIN
}
