package com.devcamp.shop24h.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name="slide")
public class Slide {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotEmpty
	@Column(name="slide_name")
	private String slideName;
	
	@Column(name="slide_url")
	private String slideUrl;

	public Slide() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Slide(int id, @NotEmpty String slideName, String slideUrl) {
		super();
		this.id = id;
		this.slideName = slideName;
		this.slideUrl = slideUrl;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSlideName() {
		return slideName;
	}

	public void setSlideName(String slideName) {
		this.slideName = slideName;
	}

	public String getSlideUrl() {
		return slideUrl;
	}

	public void setSlideUrl(String slideUrl) {
		this.slideUrl = slideUrl;
	}
	
	

}
