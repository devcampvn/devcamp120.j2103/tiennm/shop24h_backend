package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
@Entity
@Table(name="product_brand")
public class ProductBrand {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty(message = "brandName không được để trống")
	@Column(name="brand_name")
	private String brandName;
	
	@OneToMany(targetEntity = Product.class , cascade = CascadeType.ALL)
	@JoinColumn(name="product_brand_id")
	private List<Product> products;

	public ProductBrand() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductBrand(int id, @NotEmpty(message = "brandName không được để trống") String brandName,
			List<Product> products) {
		super();
		this.id = id;
		this.brandName = brandName;
		this.products = products;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
}
