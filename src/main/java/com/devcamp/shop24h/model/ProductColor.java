package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
@Entity
@Table(name="product_color")
public class ProductColor {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty(message = "color không được để trống")
	@Column(name="colorName")
	private String colorName;
	
	@OneToMany(targetEntity = Product.class , cascade = CascadeType.ALL)
	@JoinColumn(name="product_color_id")
	private List<Product> products;

	public ProductColor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductColor(int id, @NotEmpty(message = "color không được để trống") String colorName,
			List<Product> products) {
		super();
		this.id = id;
		this.colorName = colorName;
		this.products = products;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
	
	
}
