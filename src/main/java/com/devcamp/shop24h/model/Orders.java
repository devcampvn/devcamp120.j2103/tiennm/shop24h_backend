package com.devcamp.shop24h.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="orders")
public class Orders {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd-MM-yyyy")
	@Column(name="order_date" ,nullable = true )
	private Date orderDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd-MM-yyyy")
	@Column(name="required_date" ,nullable = true )
	private Date requiredDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd-MM-yyyy")
	@Column(name="shipped_date" ,nullable = true)
	private Date shippedDate;
	
	@NotEmpty(message = "status không được để trống")
	@Column(name="status")
	private String status;
	
	@Column(name="comments")
	private String comments;
	
	@ManyToOne
	@JsonIgnore
	private Customer customer;
	
	@OneToMany(targetEntity = OrderDetail.class , cascade = CascadeType.ALL)
	@JoinColumn(name="order_id")
	private List<OrderDetail> orderDetails;

	public Orders() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Orders(int id, Date orderDate, Date requiredDate, Date shippedDate,
			@NotEmpty(message = "status không được để trống") String status, String comments, Customer customer,
			List<com.devcamp.shop24h.model.OrderDetail> orderDetails) {
		super();
		this.id = id;
		this.orderDate = orderDate;
		this.requiredDate = requiredDate;
		this.shippedDate = shippedDate;
		this.status = status;
		this.comments = comments;
		this.customer = customer;
		this.orderDetails = orderDetails;
	}
	public int getCustomerId() {
		return this.customer.getId();
	}
	public String getFullName() {
		return this.customer.getFirstName() + " " + this.customer.getLastName();
	}

	public int getId() {
		return id;
	}	

	public void setId(int id) {
		this.id = id;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(Date requiredDate) {
		this.requiredDate = requiredDate;
	}

	public Date getShippedDate() {
		return shippedDate;
	}

	public void setShippedDate(Date shippedDate) {
		this.shippedDate = shippedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}

	
}
