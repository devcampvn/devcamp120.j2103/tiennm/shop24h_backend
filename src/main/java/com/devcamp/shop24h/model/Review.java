package com.devcamp.shop24h.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="review")
public class Review {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	@NotEmpty
	@Column(name="review")
	private String review;
	
	@NotNull
	@Column(name="rating")
	private int rating;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "dd-MM-yyyy")
	@Column(name="create_date" ,nullable = true )
	private Date createDate;
	
	@ManyToOne
	@JsonIgnore
	private Product product;
	
	@ManyToOne
	@JsonIgnore
	private User user;
	
	@OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL , mappedBy = "review")
	private Reply reply;

	public Review() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Review(Long id, @NotEmpty String review, @NotNull int rating, Date createDate, Product product, User user,
			Reply reply) {
		super();
		this.id = id;
		this.review = review;
		this.rating = rating;
		this.createDate = createDate;
		this.product = product;
		this.user = user;
		this.reply = reply;
	}
	
	public String getUserName() {
		return this.user.getUsername();
	}
	
	public String getProductName() {
		return this.product.getProductName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Reply getReply() {
		return reply;
	}

	public void setReply(Reply reply) {
		this.reply = reply;
	}

	
}
