package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
@Entity
@Table(name = "product_line")
public class ProductLine {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty(message = "productLine không được để trống")
	@Column(name="product_line" , unique = true)
	private String productLine;
	
	@NotEmpty(message = "ảnh không được để trống")
	@Column(name="image_productLine")
	private String imageProductLine;
	
	@Lob
	@Column(name="description" , length = 2500)
	private String description;
	
	@OneToMany(targetEntity = Product.class , cascade = CascadeType.ALL)
	@JoinColumn(name="product_line_id")
	private List<Product> products;

	public ProductLine() {
		super();
		// TODO Auto-generated constructor stub
	}


	public ProductLine(int id, @NotEmpty(message = "productLine không được để trống") String productLine,
			@NotEmpty(message = "ảnh không được để trống") String imageProductLine, String description,
			List<Product> products) {
		super();
		this.id = id;
		this.productLine = productLine;
		this.imageProductLine = imageProductLine;
		this.description = description;
		this.products = products;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}


	public String getImageProductLine() {
		return imageProductLine;
	}


	public void setImageProductLine(String imageProductLine) {
		this.imageProductLine = imageProductLine;
	}
	
	
}
