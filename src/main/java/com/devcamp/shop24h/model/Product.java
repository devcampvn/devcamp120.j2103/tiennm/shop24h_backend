package com.devcamp.shop24h.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty(message = "productCode không được để trống")
	@Column(name = "product_code" , unique = true)
	private String productCode;
	
	@NotEmpty(message = "productName không được để trống")
	@Column(name="product_name")
	private String productName;
	
	@Lob
	@Column(name="product_description" , length = 2500)
	private String productDescription;
	
	@NotNull(message = "quantityInStock không được để trống")
	@Column(name = "quantity_in_stock")
	private int quantityInStock;
	
	@NotNull(message = "price không được để trống")
	@Column(name="price")
	private BigDecimal price;
	
	@ManyToOne
	@JsonIgnore
	private ProductLine productLine;
	
	@ManyToOne
	@JsonIgnore
	private ProductBrand productBrand;
	
	
	@ManyToOne
	@JsonIgnore
	private ProductColor productColor;
	
	
	@OneToMany(targetEntity = ProductImages.class , cascade = CascadeType.ALL , fetch = FetchType.LAZY )
	@JoinColumn(name="product_id")
	private List<ProductImages> productImages;
	
	@OneToMany(targetEntity = Review.class , cascade = CascadeType.ALL , fetch = FetchType.LAZY)
	@JoinColumn(name="product_id")
	private List<Review> reviews;
	
	@OneToMany(targetEntity = OrderDetail.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
	private List<OrderDetail> orderDetails;

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	



	public Product(int id, @NotEmpty(message = "productCode không được để trống") String productCode,
			@NotEmpty(message = "productName không được để trống") String productName, String productDescription,
			@NotNull(message = "quantityInStock không được để trống") int quantityInStock,
			@NotNull(message = "price không được để trống") BigDecimal price, ProductLine productLine,
			ProductBrand productBrand, ProductColor productColor, List<ProductImages> productImages,
			List<Review> reviews, List<OrderDetail> orderDetails) {
		super();
		this.id = id;
		this.productCode = productCode;
		this.productName = productName;
		this.productDescription = productDescription;
		this.quantityInStock = quantityInStock;
		this.price = price;
		this.productLine = productLine;
		this.productBrand = productBrand;
		this.productColor = productColor;
		this.productImages = productImages;
		this.reviews = reviews;
		this.orderDetails = orderDetails;
	}


	public String getProductLineName() {
		return this.productLine.getProductLine();
	}
	
	public String getProductBrandName() {
		return this.productBrand.getBrandName();
	}
	
	public String getProductColorName() {
		return this.productColor.getColorName();
	}
	
	public int getProductLineId() {
		return this.productLine.getId();
	}
	
	public int getProductBrandId() {
		return this.productBrand.getId();
	}
	
	public int getProductColorId() {
		return this.productColor.getId();
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public String getProductCode() {
		return productCode;
	}





	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}





	public String getProductName() {
		return productName;
	}





	public void setProductName(String productName) {
		this.productName = productName;
	}





	public String getProductDescription() {
		return productDescription;
	}





	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}





	public int getQuantityInStock() {
		return quantityInStock;
	}





	public void setQuantityInStock(int quantityInStock) {
		this.quantityInStock = quantityInStock;
	}





	public BigDecimal getPrice() {
		return price;
	}





	public void setPrice(BigDecimal price) {
		this.price = price;
	}





	public ProductLine getProductLine() {
		return productLine;
	}





	public void setProductLine(ProductLine productLine) {
		this.productLine = productLine;
	}





	public ProductBrand getProductBrand() {
		return productBrand;
	}





	public void setProductBrand(ProductBrand productBrand) {
		this.productBrand = productBrand;
	}





	public ProductColor getProductColor() {
		return productColor;
	}





	public void setProductColor(ProductColor productColor) {
		this.productColor = productColor;
	}





	public List<ProductImages> getProductImages() {
		return productImages;
	}





	public void setProductImages(List<ProductImages> productImages) {
		this.productImages = productImages;
	}





	public List<Review> getReviews() {
		return reviews;
	}





	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}





	public List<OrderDetail> getOrderDetails() {
		return orderDetails;
	}





	public void setOrderDetails(List<OrderDetail> orderDetails) {
		this.orderDetails = orderDetails;
	}


	
	
}
