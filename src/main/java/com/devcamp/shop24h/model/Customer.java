package com.devcamp.shop24h.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty(message = "firstName không được để trống")
	@Column(name="first_name")
	private String firstName;
	
	@NotEmpty(message = "lastName không được để trống")
	@Column(name="last_name")
	private String lastName;
	
	@NotEmpty(message = "address không được để trống")
	@Column(name = "address")
	private String address;
	
	@NotEmpty(message = "phoneNumber không được để trống")
	private String phoneNumber;
	
	@NotEmpty(message = "email không được để trống")
	@Column(name = "email")
	private String email;
	
	@Column(name = "postal_code")
	private String postalCode;
	
	@Column(name = "cityde")
	private String city;
	
	@Column(name = "country")
	private String country;
	
	@OneToMany(targetEntity = Orders.class , cascade = CascadeType.ALL)
	@JoinColumn(name="customer_id")
	private List<Orders> orders;
	
	@OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
	@JsonIgnore
    private User user;


	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Customer(int id, @NotEmpty(message = "firstName không được để trống") String firstName,
			@NotEmpty(message = "lastName không được để trống") String lastName,
			@NotEmpty(message = "address không được để trống") String address,
			@NotEmpty(message = "phoneNumber không được để trống") String phoneNumber,
			@NotEmpty(message = "email không được để trống") String email, String postalCode, String city,
			String country, List<Orders> orders, User user) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
		this.orders = orders;
		this.user = user;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPostalCode() {
		return postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public List<Orders> getOrders() {
		return orders;
	}


	public void setOrders(List<Orders> orders) {
		this.orders = orders;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	

	
}
