package com.devcamp.shop24h.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="product_images")
public class ProductImages {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@NotEmpty
	@Column(name="image_name")
	private String imageName;
	
	@NotEmpty
	@Column(name="image_url")
	private String imageUrl;
	
	@ManyToOne
	@JsonIgnore
	private Product product;

	public ProductImages() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductImages(int id, @NotEmpty String imageName, @NotEmpty String imageUrl, Product product) {
		super();
		this.id = id;
		this.imageName = imageName;
		this.imageUrl = imageUrl;
		this.product = product;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
}
