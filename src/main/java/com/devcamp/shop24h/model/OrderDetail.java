package com.devcamp.shop24h.model;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="order_details")
public class OrderDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="price_each")
	private BigDecimal priceEach;
	
	@Column(name="quantity_order")
	private int quantityOrder;
	
	@ManyToOne
	@JsonIgnore
	private Orders order;
	
	@ManyToOne
    @JsonIgnore
	private Product product;
	
	@Transient
	private String productName;
	
	public String getProductName() {
		return getProduct().getProductName();
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}

	public OrderDetail() {
		super();
		// TODO Auto-generated constructor stub
	}


	public OrderDetail(int id, BigDecimal priceEach, int quantityOrder, Orders order, Product product) {
		super();
		this.id = id;
		this.priceEach = priceEach;
		this.quantityOrder = quantityOrder;
		this.order = order;
		this.product = product;
	}
	
	public int getProductId() {
		return this.product.getId();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public BigDecimal getPriceEach() {
		return priceEach;
	}


	public void setPriceEach(BigDecimal priceEach) {
		this.priceEach = priceEach;
	}


	public int getQuantityOrder() {
		return quantityOrder;
	}


	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}


	public Orders getOrder() {
		return order;
	}


	public void setOrder(Orders order) {
		this.order = order;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}

	
	
	
}
