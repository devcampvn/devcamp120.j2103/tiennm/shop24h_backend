package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.ProductBrand;
import com.devcamp.shop24h.repository.ProductBrandRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ProductBrandController {
	@Autowired
	private ProductBrandRepository productBrandRepository;
	
	/**
	 * Hiển thị danh sách product brand
	 */
	@CrossOrigin
	@GetMapping("/productBrand/all")
	public ResponseEntity<List<ProductBrand>> getAllProductBrand(){
		try {
			List<ProductBrand> listProductBrands = new ArrayList<ProductBrand>();
			productBrandRepository.findAll().forEach(listProductBrands::add);
			return new ResponseEntity<>(listProductBrands , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product brand theo quyền admin vs manager
	 */
	@CrossOrigin
	@GetMapping("/admin/productBrand/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<ProductBrand>> getAllProductBrandAdmin(){
		try {
			List<ProductBrand> listProductBrands = new ArrayList<ProductBrand>();
			productBrandRepository.findAll().forEach(listProductBrands::add);
			return new ResponseEntity<>(listProductBrands , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product brand theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/productBrand/{id}")
	public ResponseEntity<Object> getProductBrandById(@PathVariable("id") int id){
		try {
			Optional<ProductBrand> vProductBrand = productBrandRepository.findById(id);
			if (vProductBrand.isPresent()) {
				return new ResponseEntity<>(vProductBrand , HttpStatus.FOUND);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Tạo mới product brand
	 * @RequestBody ProductBrand vProductBrand
	 */
	@CrossOrigin
	@PostMapping("/productBrand/create")
	public ResponseEntity<Object> createProductBrand(@RequestBody ProductBrand vProductBrand){
		try {
			ProductBrand bProductBrand = new ProductBrand();
			bProductBrand.setBrandName(vProductBrand.getBrandName());
			ProductBrand saveProductBrand = productBrandRepository.save(bProductBrand);
			return new ResponseEntity<>(saveProductBrand , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified productBrand: "+e);
		}
	}
	
	/**
	 * Update product brand theo id
	 * @PathVariable id 
	 * @RequestBody ProductBrand vProductBrand
	 */
	@CrossOrigin
	@PutMapping("/productBrand/update/{id}")
	public ResponseEntity<Object> updateProductBrand(@PathVariable("id") int id , @RequestBody ProductBrand vProductBrand){
		try {
			Optional<ProductBrand> productBrandData = productBrandRepository.findById(id);
			if (productBrandData.isPresent()) {
				ProductBrand bProductBrand = productBrandData.get();
				bProductBrand.setBrandName(vProductBrand.getBrandName());
				ProductBrand saveProductBrand = productBrandRepository.save(bProductBrand);
				return new ResponseEntity<>(saveProductBrand , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified productBrand: " + id +"for update");
		}
	}
	
	/**
	 * Xoá product brand theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/productBrand/delete/{id}")
	public ResponseEntity<Object> deleteProductBrandById(@PathVariable("id") int id){
		try {
			productBrandRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
