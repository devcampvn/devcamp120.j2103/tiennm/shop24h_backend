package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.ProductLine;
import com.devcamp.shop24h.repository.ProductLineRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ProductLineController {
	@Autowired
	private ProductLineRepository productLineRepository;
	
	
	/**
	 * Hiển hị danh sách product line
	 */
	@CrossOrigin
	@GetMapping("/productline/all")
	public ResponseEntity<List<ProductLine>> getAllProductLine(){
		try {
			List<ProductLine> pProductLine = new ArrayList<ProductLine>();
			productLineRepository.findAll().forEach(pProductLine::add);
			return new ResponseEntity<>(pProductLine , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển hị danh sách product line với quyền admin vs manager
	 */
	@CrossOrigin
	@GetMapping("/admin/productline/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<ProductLine>> getAllProductLineAdmin(){
		try {
			List<ProductLine> pProductLine = new ArrayList<ProductLine>();
			productLineRepository.findAll().forEach(pProductLine::add);
			return new ResponseEntity<>(pProductLine , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị product line theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/productLine/{id}")
	public ResponseEntity<Object> getProductLineById(@PathVariable("id") int id){
		try {
			Optional<ProductLine> pProductLine = productLineRepository.findById(id);
			if (pProductLine.isPresent()) {
				return new ResponseEntity<>(pProductLine.get() , HttpStatus.FOUND);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	/**
	 * Hiển thị product line theo id với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/admin/productLine/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> getProductLineByIdAdmin(@PathVariable("id") int id){
		try {
			Optional<ProductLine> pProductLine = productLineRepository.findById(id);
			if (pProductLine.isPresent()) {
				return new ResponseEntity<>(pProductLine.get() , HttpStatus.FOUND);
			}else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	/**
	 * Thêm mới product line 
	 * @RequestBody ProductLine vProductLine
	 */
	@CrossOrigin
	@PostMapping("/productLine/create")
	public ResponseEntity<Object> createProductLine(@RequestBody ProductLine vProductLine){
		try {
			ProductLine cProductLine = new ProductLine();
			cProductLine.setProductLine(vProductLine.getProductLine());
			cProductLine.setDescription(vProductLine.getDescription());
			cProductLine.setProducts(vProductLine.getProducts());
			cProductLine.setImageProductLine(vProductLine.getImageProductLine());
			ProductLine saveProductLine = productLineRepository.save(cProductLine);
			return new ResponseEntity<>(saveProductLine , HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProductLine: "+e.getCause().getCause().getMessage());

		}
	}
	
	/**
	 * Thêm mới product line  với quyền admin
	 * @RequestBody ProductLine vProductLine
	 */
	@CrossOrigin
	@PostMapping("/admin/productLine/create")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> createProductLineAdmin(@RequestBody ProductLine vProductLine){
		try {
			ProductLine cProductLine = new ProductLine();
			cProductLine.setProductLine(vProductLine.getProductLine());
			cProductLine.setDescription(vProductLine.getDescription());
			cProductLine.setProducts(vProductLine.getProducts());
			cProductLine.setImageProductLine(vProductLine.getImageProductLine());
			ProductLine saveProductLine = productLineRepository.save(cProductLine);
			return new ResponseEntity<>(saveProductLine , HttpStatus.CREATED);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProductLine: "+e.getCause().getCause().getMessage());

		}
	}
	
	/**
	 * Cập nhât product line theo id
	 * @PathVariable id
	 * @RequestBody ProductLine vProductLine
	 */
	@CrossOrigin
	@PutMapping("/productLine/update/{id}")
	public ResponseEntity<Object> updateProductLineById(@PathVariable("id") int id , @RequestBody ProductLine vProductLine){
		try {
			Optional<ProductLine> productLineData = productLineRepository.findById(id);
			if (productLineData.isPresent()) {
				ProductLine cProductLine = productLineData.get();
				cProductLine.setProductLine(vProductLine.getProductLine());
				cProductLine.setDescription(vProductLine.getDescription());
				cProductLine.setImageProductLine(vProductLine.getImageProductLine());
				ProductLine saveProductLine = productLineRepository.save(cProductLine);
				return new ResponseEntity<>(saveProductLine , HttpStatus.OK);
			}
			else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified productLine: " + id +"for update");
		}
	}
	
	
	/**
	 * Cập nhât product line theo id với quyền admin vs manager
	 * @PathVariable id
	 * @RequestBody ProductLine vProductLine
	 */
	@CrossOrigin
	@PutMapping("/admin/productLine/update/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> updateProductLineByIdAdmin(@PathVariable("id") int id , @RequestBody ProductLine vProductLine){
		try {
			Optional<ProductLine> productLineData = productLineRepository.findById(id);
			if (productLineData.isPresent()) {
				ProductLine cProductLine = productLineData.get();
				cProductLine.setProductLine(vProductLine.getProductLine());
				cProductLine.setDescription(vProductLine.getDescription());
				cProductLine.setImageProductLine(vProductLine.getImageProductLine());
				ProductLine saveProductLine = productLineRepository.save(cProductLine);
				return new ResponseEntity<>(saveProductLine , HttpStatus.OK);
			}
			else {
				return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified productLine: " + id +"for update");
		}
	}
	
	
	/**
	 * Xoá product line theo id 
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/productLine/delete/{id}")
	public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable("id") int id){
		try {
			productLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Xoá product line theo id với quyền admin vs manager 
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/admin/productLine/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<ProductLine> deleteProductLineByIdAdmin(@PathVariable("id") int id){
		try {
			productLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
