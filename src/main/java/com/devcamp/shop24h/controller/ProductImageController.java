package com.devcamp.shop24h.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Product;
import com.devcamp.shop24h.model.ProductImages;
import com.devcamp.shop24h.repository.ProductImageRepository;
import com.devcamp.shop24h.repository.ProductRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ProductImageController {
	@Autowired
	private ProductImageRepository vProductImageRepository;
	
	@Autowired
	private ProductRepository vProductRepository;
	
	/**
	 * Hiển thị danh sách product image theo product id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/productimages/productId/{id}")
	public ResponseEntity<List<ProductImages>> getAllProductImagesByProductId(@PathVariable("id") Integer id){
		try {
			List<ProductImages> listProductImages = vProductImageRepository.findByProductId(id);
			if (listProductImages != null) {
				return new ResponseEntity<>(listProductImages , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product image theo product id với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/admin/productimages/productId/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<ProductImages>> getAllProductImagesByProductIdAdmin(@PathVariable("id") Integer id){
		try {
			List<ProductImages> listProductImages = vProductImageRepository.findByProductId(id);
			if (listProductImages != null) {
				return new ResponseEntity<>(listProductImages , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product image theo  id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/productimages/{id}")
	public ResponseEntity<Object> getProductImageById(@PathVariable("id") int id){
		try {
			Optional<ProductImages> vProductImage = vProductImageRepository.findById(id);
			if (vProductImage != null) {
				return new ResponseEntity<>(vProductImage , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * tạo mới product image theo  productID
	 * @PathVariable productId
	 * @RequestBody ProductImages vProductImage
	 */
	@CrossOrigin
	@PostMapping("productImages/create/{productId}")
	public ResponseEntity<Object> createProductImageByProductId(@PathVariable("productId") int productId , @RequestBody ProductImages vProductImage){
		try {
			Optional<Product> productData = vProductRepository.findById(productId);
			if (productData.isPresent()) {
				Product vProduct = productData.get();
				List<ProductImages> lisProductImages = vProduct.getProductImages();
				lisProductImages.add(vProductImage);
				Product saveProduct = vProductRepository.save(vProduct);
				return new ResponseEntity<>(saveProduct , HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified productImages: "+e.getCause().getCause().getMessage());
		}
	}
	
	/**
	 * tạo mới product image theo  productID với quyền admin
	 * @PathVariable productId
	 * @RequestBody ProductImages vProductImage
	 */
	@CrossOrigin
	@PostMapping("/admin/productImages/create/{productId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> createProductImageByProductIdAdmin(@PathVariable("productId") int productId , @RequestBody ProductImages vProductImage){
		try {
			Optional<Product> productData = vProductRepository.findById(productId);
			if (productData.isPresent()) {
				Product vProduct = productData.get();
				List<ProductImages> lisProductImages = vProduct.getProductImages();
				lisProductImages.add(vProductImage);
				Product saveProduct = vProductRepository.save(vProduct);
				return new ResponseEntity<>(saveProduct , HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified productImages: "+e.getCause().getCause().getMessage());
		}
	}
	
	/**
	 * cập nhật product image theo  id
	 * @PathVariable id
	 * @RequestBody ProductImages vProductImage
	 */
	@CrossOrigin
	@PutMapping("/productImages/update/{id}")
	public ResponseEntity<Object> updateProductImageById(@PathVariable("id") int id , @RequestBody ProductImages vProductImages){
		try {
			Optional<ProductImages> productImageData = vProductImageRepository.findById(id);
			if (productImageData.isPresent()) {
				ProductImages newProductImages = productImageData.get();
				newProductImages.setImageName(vProductImages.getImageName());
				newProductImages.setImageUrl(vProductImages.getImageName());
				ProductImages saveProductImages = vProductImageRepository.save(newProductImages);
				return new ResponseEntity<>(saveProductImages , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified productImage: " + id +"for update");
		}
	}
	
	/**
	 * tạo mới product image theo  productID với quyền admin vs manager
	 * @PathVariable productId
	 * @RequestBody ProductImages vProductImage
	 */
	@CrossOrigin
	@PutMapping("/admin/productImages/update/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> updateProductImageByIdAdmin(@PathVariable("id") int id , @RequestBody ProductImages vProductImages){
		try {
			Optional<ProductImages> productImageData = vProductImageRepository.findById(id);
			if (productImageData.isPresent()) {
				ProductImages newProductImages = productImageData.get();
				newProductImages.setImageName(vProductImages.getImageName());
				newProductImages.setImageUrl(vProductImages.getImageName());
				ProductImages saveProductImages = vProductImageRepository.save(newProductImages);
				return new ResponseEntity<>(saveProductImages , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified productImage: " + id +"for update");
		}
	}
	
	/**
	 * xoá product image theo  id
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/productImage/delete/{id}")
	public ResponseEntity<ProductImages> deleteProductImageById(@PathVariable("id") int id){
		try {
			vProductImageRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * xoá product image theo  id với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/admin/productImage/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<ProductImages> deleteProductImageByIdAdmin(@PathVariable("id") int id){
		try {
			vProductImageRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
