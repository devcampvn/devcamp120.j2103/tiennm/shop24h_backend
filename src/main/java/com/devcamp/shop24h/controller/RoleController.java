package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Role;
import com.devcamp.shop24h.repository.RoleRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class RoleController {
	@Autowired
	private RoleRepository roleRepository;
	
	/**
	 * Hiển thị danh sách role theo quyền admin vs manager
	 */
	@GetMapping("/roles/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Role>> getAllRoles(){
		try {
			List<Role> vRoles = new ArrayList<Role>();
			roleRepository.findAll().forEach(vRoles::add);
			return new ResponseEntity<>(vRoles , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
