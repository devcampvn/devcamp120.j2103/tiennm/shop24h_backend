package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.ProductColor;
import com.devcamp.shop24h.repository.ProductColorRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ProductColorController {
	@Autowired
	private ProductColorRepository productColorRepository;
	
	
	/**
	 * Hiển thị danh sách product color
	 */
	@CrossOrigin
	@GetMapping("/productColor/all")
	public ResponseEntity<List<ProductColor>> getAllProductColor(){
		try {
			List<ProductColor> listProductColor = new ArrayList<ProductColor>();
			productColorRepository.findAll().forEach(listProductColor::add);
			return new ResponseEntity<>(listProductColor , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product color theo quyền admin vs manager
	 */
	@CrossOrigin
	@GetMapping("/admin/productColor/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<ProductColor>> getAllProductColorAdmin(){
		try {
			List<ProductColor> listProductColor = new ArrayList<ProductColor>();
			productColorRepository.findAll().forEach(listProductColor::add);
			return new ResponseEntity<>(listProductColor , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị product color theo id
	 * @PathVariable id
	 */ 
	@CrossOrigin
	@GetMapping("/productColor/{id}")
	public ResponseEntity<Object> getProductColorById(@PathVariable("id") int id){
		try {
			Optional<ProductColor> productColorData = productColorRepository.findById(id);
			if (productColorData.isPresent()) {
				return new ResponseEntity<>(productColorData , HttpStatus.FOUND);
			}else {
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới product color 
	 * @RequestBody ProductColor vProductColor
	 */ 
	@CrossOrigin
	@PostMapping("/productColor/create")
	public ResponseEntity<Object> createProductColor(@RequestBody ProductColor vProductColor){
		try {
			ProductColor newProductColor = new ProductColor();
			newProductColor.setColorName(vProductColor.getColorName());
			ProductColor saveProductColor = productColorRepository.save(newProductColor);
			return new ResponseEntity<>(saveProductColor , HttpStatus.CREATED);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified productColor: "+ e);
		}
	}
	
	/**
	 * Cập nhật  product color theo id
	 * @PathVariable id
	 * @RequestBody ProductColor vProductColor
	 */
	@CrossOrigin
	@PutMapping("/productColor/update/{id}")
	public ResponseEntity<Object> updateProductColorById(@PathVariable("id") int id , @RequestBody ProductColor vProductColor){
		try {
			Optional<ProductColor> productColorData = productColorRepository.findById(id);
			if (productColorData.isPresent()) {
				ProductColor newProductColor = productColorData.get();
				newProductColor.setColorName(vProductColor.getColorName());
				ProductColor saveProductColor = productColorRepository.save(newProductColor);
				return new ResponseEntity<>(saveProductColor , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified productColor: " + id +"for update");
		}
	}
	
	/**
	 * Xoá product color theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/productColor/delete/{id}")
	public ResponseEntity<Object> deleteProductColorById(@PathVariable("id") int id){
		try {
			productColorRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
