package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Customer;
import com.devcamp.shop24h.model.Orders;
import com.devcamp.shop24h.output.CountOrderByDate;
import com.devcamp.shop24h.output.CountOrderByWeek;
import com.devcamp.shop24h.repository.CustomerRepository;
import com.devcamp.shop24h.repository.OrderRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class OrderController {
	@Autowired
	private OrderRepository pOrderRepository;
	
	@Autowired
	private CustomerRepository pCustomerRepository;
	
	/**
	 * Hiển thị danh sách orders
	 */
	@CrossOrigin
	@GetMapping("/orders/all")
	public ResponseEntity<List<Orders>> getAllOrder(){
		try {
			List<Orders> pOrders = new ArrayList<Orders>();
			pOrderRepository.findAll().forEach(pOrders::add);
			return new ResponseEntity<>(pOrders , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách orders với quyền admin hoặc manager
	 */
	@CrossOrigin
	@GetMapping("/admin/orders/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Orders>> getAllOrderAdmin(){
		try {
			List<Orders> pOrders = new ArrayList<Orders>();
			pOrderRepository.findAll().forEach(pOrders::add);
			return new ResponseEntity<>(pOrders , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách orders theo customer id
	 * @PathVariable customerId
	 */
	@CrossOrigin
	@GetMapping("/orders/customer/{customerId}")
	public ResponseEntity<List<Orders>> getAllOrderByCustomerId(@PathVariable("customerId") int customerId){
		try {
			List<Orders> pOrders = pOrderRepository.findOrdersByCustomerId(customerId);
			if (pOrders != null) {
				return new ResponseEntity<>(pOrders , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Đếm số orders
	 */
	@CrossOrigin
	@GetMapping("/orders/count")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<CountOrderByDate>> getCountOrderByDay(){
		try {
			List<CountOrderByDate> listOrders = pOrderRepository.getCountOrder();
			return new ResponseEntity<>(listOrders , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Đếm số orders theo tuần
	 */
	@CrossOrigin
	@GetMapping("/orders/countbyweek")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<CountOrderByWeek>> getCountOrderByWeek(){
		try {
			List<CountOrderByWeek> listOrders = pOrderRepository.getCountOrderByWeek();
			return new ResponseEntity<>(listOrders , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * tạo mới order
	 * @RequestBody Orders vOrders
	 */
	@CrossOrigin
	@PostMapping("/orders/create")
	public ResponseEntity<Object> createorders(@RequestBody Orders vOrders){
		try {
			Orders pOrders = new Orders();
			pOrders.setOrderDate(new Date());
			pOrders.setRequiredDate(vOrders.getRequiredDate());
			pOrders.setShippedDate(vOrders.getShippedDate());
			pOrders.setComments(vOrders.getComments());
			pOrders.setStatus(vOrders.getStatus());
			Orders saveOrders = pOrderRepository.save(pOrders);
			return new ResponseEntity<>(saveOrders , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orders: "+e.getCause().getCause().getMessage());
		}
	}
	
	
	/**
	 * tạo mới order theo customer Id
	 * @RequestBody Orders vOrders
	 * @PathVariable customerId
	 */
	@CrossOrigin
	@PostMapping("/orders/create/{customerId}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> createOrderByProductId(@PathVariable("customerId") int customerId , @RequestBody Orders vOrders){
		try {
			Optional<Customer> pCustomer = pCustomerRepository.findById(customerId);
			if (pCustomer.isPresent()) {
				vOrders.setOrderDate(new Date());
				vOrders.setRequiredDate(new Date());
				vOrders.setShippedDate(new Date());
				vOrders.setCustomer(pCustomer.get());
				pOrderRepository.save(vOrders);
				return new ResponseEntity<>(vOrders , HttpStatus.OK);
//				Customer vCustomer = pCustomer.get();
//				List<Orders> listOrders = vCustomer.getOrders();
//				vOrders.setOrderDate(new Date());
//				vOrders.setRequiredDate(new Date());
//				vOrders.setShippedDate(new Date());
//				listOrders.add(vOrders);
//				Customer saveCustomer = pCustomerRepository.save(vCustomer);
//				return new ResponseEntity<>(saveCustomer , HttpStatus.OK);
				
				
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orders: "+e.getMessage());
		}
	}
	
	/**
	 * tạo mới order theo customer Id theo quyền admin hoặc manager
	 * @RequestBody Orders vOrders
	 * @PathVariable customerId
	 */
	@CrossOrigin
	@PostMapping("/admin/orders/create/{customerId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> createOrderByProductIdAdmin(@PathVariable("customerId") int customerId , @RequestBody Orders vOrders){
		try {
			Optional<Customer> pCustomer = pCustomerRepository.findById(customerId);
			if (pCustomer.isPresent()) {
				vOrders.setOrderDate(new Date());
				vOrders.setRequiredDate(new Date());
				vOrders.setShippedDate(new Date());
				vOrders.setCustomer(pCustomer.get());
				pOrderRepository.save(vOrders);
				return new ResponseEntity<>(vOrders , HttpStatus.OK);
//				Customer vCustomer = pCustomer.get();
//				List<Orders> listOrders = vCustomer.getOrders();
//				vOrders.setOrderDate(new Date());
//				vOrders.setRequiredDate(new Date());
//				vOrders.setShippedDate(new Date());
//				listOrders.add(vOrders);
//				Customer saveCustomer = pCustomerRepository.save(vCustomer);
//				return new ResponseEntity<>(saveCustomer , HttpStatus.OK);
				
				
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orders: "+e.getMessage());
		}
	}
	
	/**
	 * sửa order theo Id 
	 * @RequestBody Orders vOrders
	 * @PathVariable id
	 */
	@CrossOrigin
	@PutMapping("/orders/update/{id}")
	public ResponseEntity<Object> updateOrdersById(@PathVariable("id") int id , @RequestBody Orders vOrders){
		try {
			Optional<Orders> orderData = pOrderRepository.findById(id);
			if (orderData.isPresent()) {
				Orders pOrders = orderData.get();
				pOrders.setOrderDate(vOrders.getOrderDate());
				pOrders.setRequiredDate(vOrders.getRequiredDate());
				pOrders.setShippedDate(vOrders.getShippedDate());
				pOrders.setComments(vOrders.getComments());
				pOrders.setStatus(vOrders.getStatus());
				Orders saveOrders = pOrderRepository.save(pOrders);
				return new ResponseEntity<>(saveOrders , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified order: " + id +"for update");
		}
	}
	
	/**
	 * sửa order theo Id  với quyền admin 
	 * @RequestBody Orders vOrders
	 * @PathVariable id
	 */
	@CrossOrigin
	@PutMapping("/admin/orders/update/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> updateOrdersByIdAdmin(@PathVariable("id") int id , @RequestBody Orders vOrders){
		try {
			Optional<Orders> orderData = pOrderRepository.findById(id);
			if (orderData.isPresent()) {
				Orders pOrders = orderData.get();
				pOrders.setOrderDate(vOrders.getOrderDate());
				pOrders.setRequiredDate(vOrders.getRequiredDate());
				pOrders.setShippedDate(vOrders.getShippedDate());
				pOrders.setComments(vOrders.getComments());
				pOrders.setStatus(vOrders.getStatus());
				Orders saveOrders = pOrderRepository.save(pOrders);
				return new ResponseEntity<>(saveOrders , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified order: " + id +"for update");
		}
	}
	
	/**
	 * xoá order theo Id  
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/orders/delete/{id}")
	public ResponseEntity<Orders> deleteOrderById(@PathVariable("id") int id){
		try {
			pOrderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * xoá order theo Id   với quyền admin
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/admin/orders/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Orders> deleteOrderByIdAdmin(@PathVariable("id") int id){
		try {
			pOrderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
