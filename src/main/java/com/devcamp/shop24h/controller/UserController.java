package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.ERole;
import com.devcamp.shop24h.model.Role;
import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.output.UserRole;
import com.devcamp.shop24h.repository.RoleRepository;
import com.devcamp.shop24h.repository.UserRepository;
import com.devcamp.shop24h.request.SignupRequest;
import com.devcamp.shop24h.response.MessageResponse;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class UserController {
	
	@Autowired
	UserRepository pUserRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PasswordEncoder encoder;
	
	/**
	 * Hiển thị danh sách user 
	 */
	@GetMapping("/users/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<User>> getAllUsers(){
		try {
			List<User> pUser = new ArrayList<User>();
			pUserRepository.findAll().forEach(pUser::add);
			return new ResponseEntity<>(pUser , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách user theo id
	 */
	@GetMapping("/users/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> getUserById(@PathVariable("id") Long id){
		try {
			Optional<User> pUser = pUserRepository.findById(id);
			if (pUser.isPresent()) {
				return new ResponseEntity<>(pUser.get() , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới user 
	 * @RequestBody SignupRequest signUpRequest
	 */
	@PostMapping("/users/create")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity< Object> createUser(@Valid @RequestBody SignupRequest signUpRequest){
		if (pUserRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (pUserRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getUsername(), 
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword())
							 );

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles.isEmpty()) {
			Role userRole = roleRepository.findByName(ERole.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "ROLE_ADMIN":
					Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				case "ROLE_MANAGER":
					Role modRole = roleRepository.findByName(ERole.ROLE_MANAGER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(modRole);

					break;
				default:
					Role userRole = roleRepository.findByName(ERole.ROLE_USER)
							.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		pUserRepository.save(user);

		return ResponseEntity.ok(user);
	}
	
	/**
	 * Cập nhật user theo id
	 * @PathVariable id
	 * @RequestBody User vUser
	 */
	@PutMapping("/users/update/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> updateUserById(@PathVariable("id") Long id , @RequestBody User vUser){
		try {
			Optional<User> userData = pUserRepository.findById(id);
			if (userData.isPresent()) {
				User pUser = userData.get();
				pUser.setUsername(vUser.getUsername());
				pUser.setEmail(vUser.getEmail());
				pUser.setPassword(encoder.encode(vUser.getPassword()));
				pUser.setRoles(vUser.getRoles());
				User saveUser = pUserRepository.save(pUser);
				return new ResponseEntity<>(saveUser , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified order: " + id +"for update");
		
		}
	}
	
	
	/**
	 * Xoá user theo id
	 * @PathVariable id
	 */
	@DeleteMapping("/users/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<User> deleteUserById(@PathVariable("id") Long id){
		try {
			pUserRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
