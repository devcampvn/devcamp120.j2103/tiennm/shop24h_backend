package com.devcamp.shop24h.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Reply;
import com.devcamp.shop24h.model.Review;
import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.output.UserReply;
import com.devcamp.shop24h.repository.ReplyRepository;
import com.devcamp.shop24h.repository.ReviewRepository;
import com.devcamp.shop24h.repository.UserRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ReplyController {
	
	@Autowired
	ReplyRepository pReplyRepository;
	
	@Autowired
	ReviewRepository pReviewRepository;
	
	@Autowired
	UserRepository pUserRepository;
	
	/**
	 * Hiển thị reply theo reviewId với quyền admin
	 * @PathVariable reviewId
	 */
	@GetMapping("/admin/reply/{reviewId}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> getReplyByReviewIdAdmin(@PathVariable("reviewId") Long reviewId){
		try {
			Optional<Reply> replyData = pReplyRepository.getReplyByReviewId(reviewId);
			if (replyData != null) {
				return new ResponseEntity<>(replyData , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị reply theo reviewId
	 * @PathVariable reviewId
	 */
	@GetMapping("/reply/{reviewId}")
	public ResponseEntity<Object> getReplyByReviewId(@PathVariable("reviewId") Long reviewId){
		try {
			Optional<UserReply> replyData = pReplyRepository.findReplyByReviewId(reviewId);
			if (replyData != null) {
				return new ResponseEntity<>(replyData , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Thêm mới reply theo reviewId vs userId
	 * @PathVariable reviewId
	 * @PathVariable userId
	 * @RequestBody Reply vReply
	 */
	@PostMapping("/reply/create/{reviewId}/{userId}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> createReplyByReviewId(@PathVariable("reviewId") Long reivewId , @PathVariable("userId") Long userId , @RequestBody Reply vReply){
		try {
			Optional<Review> reviewData = pReviewRepository.findById(reivewId);
			Optional<User> userData = pUserRepository.findById(userId);
			if (reviewData.isPresent() && userData.isPresent()) {
				vReply.setReview(reviewData.get());
				vReply.setUser(userData.get());
				vReply.setCreateDate(new Date());
				pReplyRepository.save(vReply);
				return new ResponseEntity<>(vReply , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified reply: "+e.getCause().getMessage());
		}
	}
	
	
	/**
	 * Cập nhật reply theo id
	 * @PathVariable id
	 * @RequestBody Reply vReply
	 */
	@PutMapping("/reply/update/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> updateReplyById(@PathVariable("id") Long id , @RequestBody Reply vReply){
		try {
			Optional<Reply> replyData = pReplyRepository.findById(id);
			if (replyData.isPresent()) {
				Reply pReply = replyData.get();
				pReply.setCreateDate(vReply.getCreateDate());
				pReply.setReply(vReply.getReply());
				Reply saveReply = pReplyRepository.save(pReply);
				return new ResponseEntity<>(saveReply , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified reply: " + id +"for update");
		}
	}
	
	/**
	 * Xoá reply theo id
	 * @PathVariable id
	 */
	@DeleteMapping("/reply/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Reply> deleteReplyById(@PathVariable("id") Long id){
		
		try {
			pReplyRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
