package com.devcamp.shop24h.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Product;
import com.devcamp.shop24h.model.Review;
import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.output.UserReview;
import com.devcamp.shop24h.repository.ProductRepository;
import com.devcamp.shop24h.repository.ReviewRepository;
import com.devcamp.shop24h.repository.UserRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ReviewController {
	
	@Autowired
	private ReviewRepository pReviewRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private UserRepository pUserRepository;
	
	/**
	 * Hiển thị list review
	 */
	@GetMapping("/review/all")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<Review>> getAllReview(){
		try {
			List<Review> pReviews = pReviewRepository.findAllReviewByIdDesc();
			return new ResponseEntity<>(pReviews , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị list review với quyền admin vs mannager
	 */
	@GetMapping("/admin/review/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Review>> getAllReviewByAdmin(){
		try {
			List<Review> pReviews = pReviewRepository.findAllReviewByIdDesc();
			return new ResponseEntity<>(pReviews , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Hiển thị review id
	 */
	@GetMapping("/review/{id}")
	public ResponseEntity<Object> getReviewById(@PathVariable("id") Long id){
		try {
			Optional<Review> pReview = pReviewRepository.findById(id);
			if (pReview.isPresent()) {
				return new ResponseEntity<>(pReview , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị review theo product id
	 * @PathVariable productId
	 */
	@GetMapping("/reviews/{productId}")
	public ResponseEntity<List<UserReview>> getReviewByProductId(@PathVariable("productId") int productId){
		try {
			List<UserReview> pReview = pReviewRepository.getAllReviewByProductId(productId);
			if (pReview != null) {
				return new ResponseEntity<>(pReview , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới review 
	 * @RequestBody Review vReview
	 */
	@PostMapping("/review/create")
	public ResponseEntity<Object> createReview(@RequestBody Review vReview){
		try {
			Review pReview = new Review();
			pReview.setReview(vReview.getReview());
			pReview.setRating(vReview.getRating());
			pReview.setCreateDate(new Date());
			Review saveReview = pReviewRepository.save(pReview);
			return new ResponseEntity<>(saveReview , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified review: "+e.getMessage());
		}
	}
	
	/**
	 * Tạo mới review theo productId vs userId
	 * @PathVariable productId
	 * @PathVariable userId
	 * @RequestBody Review vReview
	 */
	@PostMapping("/review/create/{productId}/{userId}")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Object> createReviewByProductIdAndUserId(@PathVariable("productId") int productId , @PathVariable("userId") Long userId , @RequestBody Review vReview){
		try {
			Optional<Product> productData = productRepository.findById(productId);
			Optional<User> userData = pUserRepository.findById(userId);
			if (productData.isPresent() && userData.isPresent()) {
				vReview.setProduct(productData.get());
				vReview.setUser(userData.get());
				vReview.setCreateDate(new Date());
				pReviewRepository.save(vReview);
				return new ResponseEntity<>(vReview , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified review: "+e.getMessage());
		}
	}
	
	/**
	 * Tạo mới review theo id
	 * @PathVariable id
	 * @RequestBody Review vReview
	 */
	@PutMapping("/review/update/{id}")
	public ResponseEntity<Object> updateReviewById(@PathVariable("id") Long id , @RequestBody Review vReview){
		try {
			Optional<Review> reviewData = pReviewRepository.findById(id);
			if (reviewData.isPresent()) {
				Review pReview = reviewData.get();
				pReview.setReview(vReview.getReview());
				pReview.setCreateDate(vReview.getCreateDate());
				pReview.setRating(vReview.getRating());
				Review saveReview = pReviewRepository.save(pReview);
				return new ResponseEntity<>(saveReview , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified review: " + id +"for update");
		}
	}
	
	/**
	 * Xoá review theo id
	 * @PathVariable id
	 */
	@DeleteMapping("/review/delete/{id}")
	public ResponseEntity<Review> deleteReviewById(@PathVariable("id") Long id){
		try {
			pReviewRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Xoá review theo id
	 * @PathVariable id
	 */
	@DeleteMapping("/admin/review/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Review> deleteReviewByIdAdmin(@PathVariable("id") Long id){
		try {
			pReviewRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/review/count/5/{productId}")
	public Long countRatingFive(@PathVariable("productId") int productId) {

		Long countStar = pReviewRepository.countRatingByFive(productId);
		return countStar;
		
	}
	
	@GetMapping("/review/count/4/{productId}")
	public Long countRatingFour(@PathVariable("productId") int productId) {

		Long countStar = pReviewRepository.countRatingByFour(productId);
		return countStar;
		
	}
	
	@GetMapping("/review/count/3/{productId}")
	public Long countRatingThree(@PathVariable("productId") int productId) {

		Long countStar = pReviewRepository.countRatingByThree(productId);
		return countStar;
		
	}
	
	@GetMapping("/review/count/2/{productId}")
	public Long countRatingTwo(@PathVariable("productId") int productId) {

		Long countStar = pReviewRepository.countRatingByTwo(productId);
		return countStar;
		
	}
	
	@GetMapping("/review/count/1/{productId}")
	public Long countRatingOne(@PathVariable("productId") int productId) {

		Long countStar = pReviewRepository.countRatingByOne(productId);
		return countStar;
		
	}
	
	@GetMapping("/review/count/{productId}")
	public Long countRating(@PathVariable("productId") int productId) {

		Long countStar = pReviewRepository.countRating(productId);
		return countStar;
		
	}
	
}
