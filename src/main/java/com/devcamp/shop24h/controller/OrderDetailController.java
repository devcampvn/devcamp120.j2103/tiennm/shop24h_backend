package com.devcamp.shop24h.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.OrderDetail;
import com.devcamp.shop24h.model.Orders;
import com.devcamp.shop24h.model.Product;
import com.devcamp.shop24h.output.FilterCustomer;
import com.devcamp.shop24h.repository.OrderDetailRepository;
import com.devcamp.shop24h.repository.OrderRepository;
import com.devcamp.shop24h.repository.ProductRepository;
import com.devcamp.shop24h.service.ExcelExporter;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class OrderDetailController {
	@Autowired
	OrderDetailRepository pOrderDetailRepository;
	
	@Autowired
	OrderRepository pOrderRepository;
	
	@Autowired
	ProductRepository productRepository;
	
	/**
	 * Hiển thị danh sách orderDetail
	 */
	@CrossOrigin
	@GetMapping("/orderdetails/all")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetail(){
		try {
			List<OrderDetail> pOrderDetails = new ArrayList<OrderDetail>();
			pOrderDetailRepository.findAll().forEach(pOrderDetails::add);
			return new ResponseEntity<>(pOrderDetails , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách orderDetail theo productId
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/orderDetail/product/{id}")
	public ResponseEntity<List<OrderDetail>> getOrderDetailByProductId(@PathVariable("id") Integer id){
		try {
			List<OrderDetail> pOrderDetails = pOrderDetailRepository.findOrderDetailByProductId(id);
			if (pOrderDetails != null) {
				return new ResponseEntity<>(pOrderDetails , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Hiển thị danh sách orderDetail theo orderId
	 * @PathVariable orderId
	 */
	@CrossOrigin
	@GetMapping("/orderdetails/order/{orderId}")
	public ResponseEntity<List<OrderDetail>> getOrderDetailByOrderId(@PathVariable("orderId") int id){
		try {
			List<OrderDetail> listOrderDetails = pOrderDetailRepository.findByOrderId(id);
			if (listOrderDetails != null) {
				return new ResponseEntity<>(listOrderDetails, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	
	/**
	 * Hiển thị danh sách orderDetail theo orderId với quyền admin hoặc manager
	 * @PathVariable orderId
	 */
	@CrossOrigin
	@GetMapping("/admin/orderdetails/order/{orderId}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<OrderDetail>> getOrderDetailByOrderIdAdmin(@PathVariable("orderId") int id){
		try {
			List<OrderDetail> listOrderDetails = pOrderDetailRepository.findByOrderId(id);
			if (listOrderDetails != null) {
				return new ResponseEntity<>(listOrderDetails, HttpStatus.OK);
			}else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Tìm kiếm khách hàng loại vip
	 */
	@CrossOrigin
	@GetMapping("/findCustomer/vip")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<FilterCustomer>> getCustomerVip(){
		try {
			List<FilterCustomer> listCustomer = pOrderDetailRepository.findCustomerVip();
			if (listCustomer != null) {
				return new ResponseEntity<>(listCustomer , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tìm kiếm khách hàng loại silver
	 */
	@CrossOrigin
	@GetMapping("/findCustomer/silver")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<FilterCustomer>> getCustomerSilver(){
		try {
			List<FilterCustomer> listCustomer = pOrderDetailRepository.findCustomerSilver();
			if (listCustomer != null) {
				return new ResponseEntity<>(listCustomer , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Tìm kiếm khách hàng loại gold
	 */
	@CrossOrigin
	@GetMapping("/findCustomer/gold")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<FilterCustomer>> getCustomerGold(){
		try {
			List<FilterCustomer> listCustomer = pOrderDetailRepository.findCustomerGold();
			if (listCustomer != null) {
				return new ResponseEntity<>(listCustomer , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Tìm kiếm khách hàng loại diamond
	 */
	@CrossOrigin
	@GetMapping("/findCustomer/diamond")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<FilterCustomer>> getCustomerDiamond(){
		try {
			List<FilterCustomer> listCustomer = pOrderDetailRepository.findCustomerDiamond();
			if (listCustomer != null) {
				return new ResponseEntity<>(listCustomer , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Xuất file khách hàng loại vip ra excel
	 */
	@CrossOrigin
	@GetMapping("/export/customerVip/excel")	
	public void exportToExcelCustomerVip(HttpServletResponse response) throws IOException{
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=customerVip_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<FilterCustomer> listCustomers = pOrderDetailRepository.findCustomerVip();
		ExcelExporter excelExporter = new ExcelExporter(listCustomers);
		excelExporter.export(response);
	}
	
	/**
	 * Xuất file khách hàng loại silver ra excel
	 */
	@CrossOrigin
	@GetMapping("/export/customerSilver/excel")	
	public void exportToExcelCustomerSilver(HttpServletResponse response) throws IOException{
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=customerSilver_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<FilterCustomer> listCustomers = pOrderDetailRepository.findCustomerSilver();
		ExcelExporter excelExporter = new ExcelExporter(listCustomers);
		excelExporter.export(response);
	}
	
	/**
	 * Xuất file khách hàng loại gold ra excel
	 */
	@CrossOrigin
	@GetMapping("/export/customerGold/excel")
	public void exportToExcelCustomerGold(HttpServletResponse response) throws IOException{
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=customerGold_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<FilterCustomer> listCustomers = pOrderDetailRepository.findCustomerGold();
		ExcelExporter excelExporter = new ExcelExporter(listCustomers);
		excelExporter.export(response);
	}
	
	/**
	 * Xuất file khách hàng loại diamond ra excel
	 */
	@CrossOrigin
	@GetMapping("/export/customerDiamond/excel")
	public void exportToExcelCustomerDiamond(HttpServletResponse response) throws IOException{
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=customerDiamond_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<FilterCustomer> listCustomers = pOrderDetailRepository.findCustomerDiamond();
		ExcelExporter excelExporter = new ExcelExporter(listCustomers);
		excelExporter.export(response);
	}
	
	/**
	 * Thêm mới orderDetail theo orderId vs productId
	 * @PathVariable orderId
	 * @PathVariable productId
	 * @RequestBody OrderDetail pOrderDetail
	 */
	@CrossOrigin
	@PostMapping("/orderdetails/order/create/{orderId}/{productId}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> createOrderDetailByOrderId(@PathVariable("orderId") int id, @PathVariable("productId") int productId,@RequestBody OrderDetail pOrderDetail){
		try {
			Optional<Orders> orderData= pOrderRepository.findById(id);
			Optional<Product> productData=  productRepository.findById(productId);
			if (orderData.isPresent()) {
				Orders newOrder = orderData.get();
				List<OrderDetail> listOrderDetails = newOrder.getOrderDetails();
			
				pOrderDetail.setOrder(newOrder);
				pOrderDetail.setProduct(productData.get());
				
				listOrderDetails.add(pOrderDetail);
			
				newOrder.setOrderDetails(listOrderDetails);
				Orders saveOrders = pOrderRepository.save(newOrder);
				return new ResponseEntity<>(saveOrders , HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(null , HttpStatus.BAD_GATEWAY);
			}	
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orderDetail: "+e.getCause().getCause().getMessage());
		}

	}
	
	/**
	 * Thêm mới orderDetail theo productId
	 * @PathVariable id
	 * @RequestBody OrderDetail pOrderDetail
	 */
	@CrossOrigin
	@PostMapping("/orderdetails/product/create/{productId}")
	public ResponseEntity<Object> createOrderDetailByProductId(@PathVariable("id") int id , @RequestBody OrderDetail pOrderDetail){
		try {
			Optional<Product> productData = productRepository.findById(id);
			if (productData.isPresent()) {
				Product bProduct = productData.get();

				Product saveProduct = productRepository.save(bProduct);
				return new ResponseEntity<>(saveProduct , HttpStatus.CREATED);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orderDetail: "+e.getCause().getCause().getMessage());
		}
	}
	
	
	/**
	 * Cập nhật orderDetail theo id
	 * @PathVariable id
	 * @RequestBody OrderDetail pOrderDetail
	 */
	@CrossOrigin
	@PutMapping("/orderdetails/update/{id}")
	public ResponseEntity<Object> updateOrderDetailById(@PathVariable("id") int id , @RequestBody OrderDetail pOrderDetail ){
		try {
			Optional<OrderDetail> orderDetailData = pOrderDetailRepository.findById(id);
			if (orderDetailData.isPresent()) {
				OrderDetail vOrderDetail = orderDetailData.get();
				vOrderDetail.setPriceEach(pOrderDetail.getPriceEach());
				vOrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
				OrderDetail saveOrderDetail = pOrderDetailRepository.save(vOrderDetail);
				return new ResponseEntity<>(saveOrderDetail , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified orderDetail: " + id +"for update");
		}
	}
	
	/**
	 * Xoá orderDetail theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/orderdetails/delete/{id}")
	public ResponseEntity<OrderDetail> deleteOrderDetailById(@PathVariable("id") int id){
		try {
			pOrderDetailRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}

