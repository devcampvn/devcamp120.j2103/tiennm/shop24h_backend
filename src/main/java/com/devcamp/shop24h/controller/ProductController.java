package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Product;
import com.devcamp.shop24h.model.ProductBrand;
import com.devcamp.shop24h.model.ProductColor;
import com.devcamp.shop24h.model.ProductLine;
import com.devcamp.shop24h.output.FilterProductList;
import com.devcamp.shop24h.output.ProductHandle;
import com.devcamp.shop24h.output.RelateProduct;
import com.devcamp.shop24h.output.RequestProductList;
import com.devcamp.shop24h.output.RequestProductPriceList;
import com.devcamp.shop24h.repository.ProductBrandRepository;
import com.devcamp.shop24h.repository.ProductColorRepository;
import com.devcamp.shop24h.repository.ProductLineRepository;
import com.devcamp.shop24h.repository.ProductRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class ProductController {
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private ProductLineRepository productLineRepository;
	
	@Autowired
	private ProductBrandRepository productBrandRepository;
	
	@Autowired ProductColorRepository productColorRepository;
	
	
	/**
	 * Hiển thị danh sách product 
	 */
	@CrossOrigin
	@GetMapping("/product/all")
	public ResponseEntity<Set<Product>> getAllProduct(){
		try {
			Set<Product> vProducts = new HashSet<Product>();
			productRepository.findAll().forEach(vProducts::add);
			return new ResponseEntity<>(vProducts , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product theo admin vs manager
	 */
	@CrossOrigin
	@GetMapping("/admin/product/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Product>> getAllProductAdmin(){
		try {
			List<Product> bProducts = new ArrayList<Product>();
			productRepository.findAll().forEach(bProducts::add);
			return new ResponseEntity<>(bProducts , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e);
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Đếm số sản phẩm 
	 */
	@CrossOrigin
	@GetMapping("/productLine/productCount")
	public ResponseEntity<List<ProductHandle>> getProductLineCount(){
		try {
			List<ProductHandle> productCount = productRepository.getProductCount();
			return new ResponseEntity<>(productCount , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	/**
	 * Hiển thị danh sách product theo thứ tự giảm dần
	 */
	@CrossOrigin
	@GetMapping("/product")
	public ResponseEntity<List<Product>> getAllProductDesc(){
		try {
			List<Product> listProducts = productRepository.getAllProductDESC();
			return new ResponseEntity<>(listProducts , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	/**
	 * Hiển thị danh sách product của giày nam
	 */
	@CrossOrigin
	@GetMapping("/product/manshoes")
	public ResponseEntity<List<Product>> getManShoes(){
		try {
			List<Product> bProduct = productRepository.getAllManShoes();
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product của giày nữ
	 */
	@CrossOrigin
	@GetMapping("/product/womanshoes")
	public ResponseEntity<List<Product>> getWomanShoes(){
		try {
			List<Product> bProduct = productRepository.getAllWomanShoes();
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product theo product color id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/product/productColor/{id}")
	public ResponseEntity<List<Product>> getProductByProductColorId(@PathVariable("id") int id){
		try {
			List<Product> bProduct = productRepository.findProductByProductColorId(id);
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product theo product brand id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/product/productBrand/{id}")
	public ResponseEntity<List<Product>> getProductByProductBrandId(@PathVariable("id") int id){
		try {
			List<Product> bProduct = productRepository.findProductByProductBrandId(id);
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product theo product line id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/product/productLine/{id}")
	public ResponseEntity<List<Product>> getProductByProductLineId(@PathVariable("id") int id){
		try {
			List<Product> bProduct = productRepository.findProductByProductLineId(id);
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách product theo product line id với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/admin/product/productLine/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Product>> getProductByProductLineIdAdmin(@PathVariable("id") int id){
		try {
			List<Product> bProduct = productRepository.findProductByProductLineId(id);
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@CrossOrigin
	@GetMapping("/product/sortProduct/{startRow}")
	public ResponseEntity<List<Product>> sortProductToLoadPage(@PathVariable("startRow") int startRow){		
		try {
			List<Product> bProduct = productRepository.sortProductByNumber(startRow);
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách sản phẩm liên quan
	 * @RequestParam productLineId
	 * @RequestParam productId
	 */
	@CrossOrigin
	@GetMapping("/relateProducts")
	public ResponseEntity<List<Product>> getProductByProductLineIdOffsetProductId(@RequestParam("productLineId") int productLineId , @RequestParam("offsetId") int productId){		
		try {
			List<Product> bProduct = productRepository.findProductByProductLineIdOffsetProductId(productLineId, productId);
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách sản phẩm có phân trang
	 */
	@CrossOrigin
	@GetMapping("/products")
	public ResponseEntity<Page<Product>> getAllProductPageable(@RequestParam("start") int start,@RequestParam("end") int end){		
		try {
			Page<Product> bProduct = productRepository.getAllProductPageable(PageRequest.of(start, end));
			return new ResponseEntity<>(bProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Đếm số sản phẩm
	 */
	@CrossOrigin
	@GetMapping("/product/count")
	public Long getCountProduct(){		
		try {
			Long countProduct = productRepository.count();
			return countProduct;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	/**
	 * Hiển thị sản phẩm theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/products/{id}")
	public ResponseEntity<Object> getProductById(@PathVariable("id") int id){
		try {
			Optional<Product> vProduct = productRepository.findById(id);
			if (vProduct.isPresent()) {
				return new ResponseEntity<>(vProduct , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Hiển thị list sản phẩm  theo id
	 */
	@CrossOrigin
	@PostMapping("/productList")
	public ResponseEntity<List<Product>> getProductById(@RequestBody RequestProductList requestProduct){
		try {
			 
			 return new ResponseEntity<>(productRepository.findProducttByIds(requestProduct.getIds()) , HttpStatus.OK);
			 
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Hiển thị list sản phẩm  theo giá
	 */
	@CrossOrigin
	@PostMapping("/productPriceList")
	public ResponseEntity<Page<Product>> getProductByPrice(@RequestBody RequestProductPriceList requestProduct , @RequestParam("start") int start,@RequestParam("end") int end){
		try {			
			return new ResponseEntity<>(productRepository.findProductByPrice(requestProduct.getMinPrice(),requestProduct.getMaxPrice() , PageRequest.of(start, end)), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Lọc sản phẩm theo product color vs product brand id có phân trang
	 * @RequestBody FilterProductList requestProduct
	 */
	@CrossOrigin
	@PostMapping("/filterProducts")
	public ResponseEntity<Page<Product>> getProductByProductLineIdAndProductColorIdAndProductBrandId(@RequestBody FilterProductList requestProduct , @RequestParam("start") int start,@RequestParam("end") int end){
		try {
			Page<Product> lisProducts = productRepository.findProductByProductLineAndBrandAndColorId(requestProduct.getProductLineIds() , requestProduct.getProductBrandIds() , requestProduct.getProductColorIds(), PageRequest.of(start, end));
			return new ResponseEntity<>(lisProducts, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới sản phẩm theo product line id, product brand id vs productColorId
	 * @PathVariable productLineId
	 * @PathVariable productBrandId
	 * @PathVariable productColorid
	 * @RequestBody Product vProduct
	 */
	@CrossOrigin
	@PostMapping("/product/create/{productLineId}/{productBrandId}/{productColorid}")
	public ResponseEntity<Object> createProductByProductLineId(@PathVariable("productLineId") int productLineId ,@PathVariable("productBrandId") int productBrandId,@PathVariable("productColorid") int productColorid, @RequestBody Product vProduct){
		try {
			Optional<ProductLine> productLineData = productLineRepository.findById(productLineId);
			Optional<ProductBrand> productBrandData = productBrandRepository.findById(productBrandId);
			Optional<ProductColor> productColorData = productColorRepository.findById(productColorid);
			if (productLineData.isPresent() && productBrandData.isPresent() && productColorData.isPresent()) {
				ProductLine vProductLine = productLineData.get();
				vProduct.setProductLine(vProductLine);
				vProduct.setProductBrand(productBrandData.get());
				vProduct.setProductColor(productColorData.get());
				productRepository.save(vProduct);
				return new ResponseEntity<>(vProduct , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới sản phẩm theo product line id, product brand id vs productColorId với quyền admin
	 * @PathVariable productLineId
	 * @PathVariable productBrandId
	 * @PathVariable productColorid
	 * @RequestBody Product vProduct
	 */
	@CrossOrigin
	@PostMapping("/admin/product/create/{productLineId}/{productBrandId}/{productColorid}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> createProductByProductLineIdAdmin(@PathVariable("productLineId") int productLineId ,@PathVariable("productBrandId") int productBrandId,@PathVariable("productColorid") int productColorid, @RequestBody Product vProduct){
		try {
			Optional<ProductLine> productLineData = productLineRepository.findById(productLineId);
			Optional<ProductBrand> productBrandData = productBrandRepository.findById(productBrandId);
			Optional<ProductColor> productColorData = productColorRepository.findById(productColorid);
			if (productLineData.isPresent() && productBrandData.isPresent() && productColorData.isPresent()) {
				ProductLine vProductLine = productLineData.get();
				vProduct.setProductLine(vProductLine);
				vProduct.setProductBrand(productBrandData.get());
				vProduct.setProductColor(productColorData.get());
				productRepository.save(vProduct);
				return new ResponseEntity<>(vProduct , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới sản phẩm 
	 * @RequestBody Product vProduct
	 */
	@CrossOrigin
	@PostMapping("/product/create")
	public ResponseEntity<Object> createProduct(@RequestBody Product vProduct){
		try {
			Product bProduct = new Product();
			bProduct.setProductName(vProduct.getProductName());
			bProduct.setProductCode(vProduct.getProductCode());
			bProduct.setProductImages(vProduct.getProductImages());
			bProduct.setProductDescription(vProduct.getProductDescription());
			bProduct.setProductBrand(vProduct.getProductBrand());
			bProduct.setProductColor(vProduct.getProductColor());
			bProduct.setPrice(vProduct.getPrice());
			Product saveProduct = productRepository.save(bProduct);
			return new ResponseEntity<>(saveProduct , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified product: "+e.getMessage());
		}
	}
	
	/**
	 * Cập nhật sản phẩm theo product line id, product brand id vs productColorId
	 * @PathVariable productLineId
	 * @PathVariable productBrandId
	 * @PathVariable productColorid
	 * @RequestBody Product vProduct
	 */
	@CrossOrigin
	@PutMapping("/product/update/{id}/{productLineId}/{productBrandId}/{productColorid}")
	public ResponseEntity<Object> updateProductById(@PathVariable("id") int id,@PathVariable("productLineId") int productLineId ,@PathVariable("productBrandId") int productBrandId,@PathVariable("productColorid") int productColorid , @RequestBody Product vProduct){
		try {
			Optional<Product> productData = productRepository.findById(id);
			Optional<ProductLine> productLineData = productLineRepository.findById(productLineId);
			Optional<ProductBrand> productBrandData = productBrandRepository.findById(productBrandId);
			Optional<ProductColor> productColorData = productColorRepository.findById(productColorid);
			if (productData.isPresent() && productLineData.isPresent() && productBrandData.isPresent() && productColorData.isPresent()) {
				Product bProduct = productData.get();
				ProductLine vProductLine = productLineData.get();
				ProductColor vProductColor = productColorData.get();
				ProductBrand vProductBrand = productBrandData.get();
				vProduct.setProductLine(vProductLine);
				vProduct.setProductColor(vProductColor);
				vProduct.setProductBrand(vProductBrand);
				bProduct.setProductName(vProduct.getProductName());
				bProduct.setProductCode(vProduct.getProductCode());
				bProduct.setProductImages(vProduct.getProductImages());
				bProduct.setProductDescription(vProduct.getProductDescription());
				bProduct.setProductBrand(vProduct.getProductBrand());
				bProduct.setProductColor(vProduct.getProductColor());
				bProduct.setPrice(vProduct.getPrice());
				bProduct.setProductLine(vProduct.getProductLine());
				bProduct.setProductColor(vProduct.getProductColor());
				bProduct.setProductBrand(vProduct.getProductBrand());
				
				Product saveProduct = productRepository.save(bProduct);
				return new ResponseEntity<>(saveProduct , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified order: " + id +"for update");
		}
	}
	
	/**
	 * Cập nhât sản phẩm theo product line id, product brand id vs productColorId với quyền admin vs manager
	 * @PathVariable productLineId
	 * @PathVariable productBrandId
	 * @PathVariable productColorid
	 * @RequestBody Product vProduct
	 */
	@CrossOrigin
	@PutMapping("/admin/product/update/{id}/{productLineId}/{productBrandId}/{productColorid}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> updateProductByIdAdmin(@PathVariable("id") int id,@PathVariable("productLineId") int productLineId ,@PathVariable("productBrandId") int productBrandId,@PathVariable("productColorid") int productColorid , @RequestBody Product vProduct){
		try {
			Optional<Product> productData = productRepository.findById(id);
			Optional<ProductLine> productLineData = productLineRepository.findById(productLineId);
			Optional<ProductBrand> productBrandData = productBrandRepository.findById(productBrandId);
			Optional<ProductColor> productColorData = productColorRepository.findById(productColorid);
			if (productData.isPresent() && productLineData.isPresent() && productBrandData.isPresent() && productColorData.isPresent()) {
				Product bProduct = productData.get();
				ProductLine vProductLine = productLineData.get();
				ProductColor vProductColor = productColorData.get();
				ProductBrand vProductBrand = productBrandData.get();
				vProduct.setProductLine(vProductLine);
				vProduct.setProductColor(vProductColor);
				vProduct.setProductBrand(vProductBrand);
				bProduct.setProductName(vProduct.getProductName());
				bProduct.setProductCode(vProduct.getProductCode());
				bProduct.setProductImages(vProduct.getProductImages());
				bProduct.setProductDescription(vProduct.getProductDescription());
				bProduct.setProductBrand(vProduct.getProductBrand());
				bProduct.setProductColor(vProduct.getProductColor());
				bProduct.setPrice(vProduct.getPrice());
				bProduct.setProductLine(vProduct.getProductLine());
				bProduct.setProductColor(vProduct.getProductColor());
				bProduct.setProductBrand(vProduct.getProductBrand());
				
				Product saveProduct = productRepository.save(bProduct);
				return new ResponseEntity<>(saveProduct , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified order: " + id +"for update");
		}
	}
	
	/**
	 * Xoá sản phẩm theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/product/delete/{id}")
	public ResponseEntity<Product> deleteProductById(@PathVariable("id") int id){
		try {
			productRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Xoá sản phẩm theo id với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/admin/product/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Product> deleteProductByIdAdmin(@PathVariable("id") int id){
		try {
			productRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
