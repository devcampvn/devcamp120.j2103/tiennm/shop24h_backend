package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.shop24h.model.Slide;
import com.devcamp.shop24h.repository.SlideRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class SlideController {
	
	@Autowired
	private SlideRepository pSlideRepository;
	
	/**
	 * Hiển thị danh sách slide
	 */
	@CrossOrigin
	@GetMapping("/slide/all")
	public ResponseEntity<List<Slide>> getAllSlides(){
		try {
			List<Slide> listSlides = new ArrayList<Slide>();
			pSlideRepository.findAll().forEach(listSlides::add);
			return new ResponseEntity<>(listSlides , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị danh sách slide với quyền admin vs manager
	 */
	@CrossOrigin
	@GetMapping("/admin/slide/all")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<Slide>> getAllSlidesAdmin(){
		try {
			List<Slide> listSlides = new ArrayList<Slide>();
			pSlideRepository.findAll().forEach(listSlides::add);
			return new ResponseEntity<>(listSlides , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị slide theo id
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/slide/{id}")
	public ResponseEntity<Object> getSlideById(@PathVariable("id") int id){
		try {
			Optional<Slide> vSlide = pSlideRepository.findById(id);
			if (vSlide.isPresent()) {
				return new ResponseEntity<>(vSlide.get() , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Hiển thị slide theo id với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@GetMapping("/admin/slide/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> getSlideByIdAdmin(@PathVariable("id") int id){
		try {
			Optional<Slide> vSlide = pSlideRepository.findById(id);
			if (vSlide.isPresent()) {
				return new ResponseEntity<>(vSlide.get() , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Tạo mới slide 
	 */
	@CrossOrigin
	@PostMapping("/slide/create")
	public ResponseEntity<Object> createSlide(@RequestBody Slide vSlide){
		try {
			Slide newSlide = new Slide();
			newSlide.setSlideName(vSlide.getSlideName());
			newSlide.setSlideUrl(vSlide.getSlideUrl());
			Slide saveSlide = pSlideRepository.save(newSlide);
			return new ResponseEntity<>(saveSlide , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orders: "+e.getMessage());
		}
	}
	
	/**
	 * Tạo mới slide với quyền admin
	 * @RequestBody Slide vSlide
	 */
	@CrossOrigin
	@PostMapping("/admin/slide/create")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> createSlideAdmin(@RequestBody Slide vSlide){
		try {
			Slide newSlide = new Slide();
			newSlide.setSlideName(vSlide.getSlideName());
			newSlide.setSlideUrl(vSlide.getSlideUrl());
			Slide saveSlide = pSlideRepository.save(newSlide);
			return new ResponseEntity<>(saveSlide , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified orders: "+e.getMessage());
		}
	}
	
	/**
	 * Cập nhật slide theo id 
	 * @PathVariable id
	 * @RequestBody Slide vSlide
	 */
	@CrossOrigin
	@PutMapping("slide/update/{id}")
	public ResponseEntity<Object> updateSlideById(@PathVariable("id") int id , @RequestBody Slide vSlide){
		try {
			Optional<Slide> pSlide = pSlideRepository.findById(id);
			if (pSlide.isPresent()) {
				Slide newSlide = pSlide.get();
				newSlide.setSlideName(vSlide.getSlideName());
				newSlide.setSlideUrl(vSlide.getSlideUrl());
				Slide saveSlide = pSlideRepository.save(newSlide);
				return new ResponseEntity<>(saveSlide , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified slide: " + id +"for update");
		}
	}
	
	/**
	 * Xoá slide theo id 
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/slide/delete/{id}")
	public ResponseEntity<Slide> deleteSlideById(@PathVariable("id") int id){
		try {
			pSlideRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Xoá slide theo id  với quyền admin vs manager
	 * @PathVariable id
	 */
	@CrossOrigin
	@DeleteMapping("/admin/slide/delete/{id}")
	@PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')")
	public ResponseEntity<Slide> deleteSlideByIdAdmin(@PathVariable("id") int id){
		try {
			pSlideRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
