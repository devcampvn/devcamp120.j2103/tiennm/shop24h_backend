package com.devcamp.shop24h.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.shop24h.model.Customer;
import com.devcamp.shop24h.model.User;
import com.devcamp.shop24h.output.ListOrder;
import com.devcamp.shop24h.repository.CustomerRepository;
import com.devcamp.shop24h.repository.UserRepository;

@CrossOrigin(origins = "*" , maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class CustomerController {
	@Autowired
	private CustomerRepository pCustomerRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	
	/**
	 * Hiển thị danh sách customer 
	 */
	@CrossOrigin
	@GetMapping("/customer/all")
	public ResponseEntity<List<Customer>> getAllCustomers(){
		try {
			List<Customer> pCustomers = new ArrayList<Customer>();
			pCustomerRepository.findAll().forEach(pCustomers::add);
			return new ResponseEntity<>(pCustomers , HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(null , HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	/**
	 * Hiển thị khách hàng theo id 
	 * @param id
	 */
	@CrossOrigin
	@GetMapping("/customer/{id}")
	public ResponseEntity<Object> getCustomerById(@PathVariable("id") int id){
		try {
			Optional<Customer> pCustomer = pCustomerRepository.findById(id);
			if (pCustomer.isPresent()) {
				return new ResponseEntity<>(pCustomer , HttpStatus.FOUND);
			}
			else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị khách hàng theo email
	 * @RequestParam email
	 */
	@CrossOrigin
	@GetMapping("/customer")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> getCustomerByEmail(@RequestParam("email") String email){
		try {
			Optional<Customer> vCustomer = pCustomerRepository.findCustomerByEmail(email);
			if (vCustomer.isPresent()) {
				return new ResponseEntity<>(vCustomer.get() , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Hiển thị list Order của khách hàng theo email
	 * @RequestParam email
	 */
	@CrossOrigin
	@GetMapping("/user/order")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<List<ListOrder>> getAllOrderUser(@RequestParam("email") String email){
		try {
			List<ListOrder> vListOrder = pCustomerRepository.getAllOrdersUser(email);
			if (vListOrder != null) {
				return new ResponseEntity<>(vListOrder , HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null , HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Tạo mới khách hàng
	 * @RequestBody Customer
	 */
	@CrossOrigin
	@PostMapping("/customer/create")
	public ResponseEntity<Object> createCustomer(@RequestBody Customer pCustomer){
		try {
			Optional<Customer> existCustomer = pCustomerRepository.findCustomerByEmail(pCustomer.getEmail());
			
			if(existCustomer.isPresent()) {
				Customer newCustomer = existCustomer.get();
				
				newCustomer.setFirstName(pCustomer.getFirstName());
				newCustomer.setLastName(pCustomer.getLastName());
				newCustomer.setAddress(pCustomer.getAddress());
				newCustomer.setEmail(pCustomer.getEmail());
				newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
				newCustomer.setCity(pCustomer.getCity());
				newCustomer.setCountry(pCustomer.getCountry());
				newCustomer.setPostalCode(pCustomer.getPostalCode());
				Customer saveCustomer = pCustomerRepository.save(newCustomer);
				return new ResponseEntity<>(saveCustomer , HttpStatus.OK);
			} else {
				Customer newCustomer = new Customer();
				newCustomer.setFirstName(pCustomer.getFirstName());
				newCustomer.setLastName(pCustomer.getLastName());
				newCustomer.setAddress(pCustomer.getAddress());
				newCustomer.setEmail(pCustomer.getEmail());
				
				newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
				newCustomer.setCity(pCustomer.getCity());
				newCustomer.setCountry(pCustomer.getCountry());
				newCustomer.setPostalCode(pCustomer.getPostalCode());
				Customer saveCustomer = pCustomerRepository.save(newCustomer);
				return new ResponseEntity<>(saveCustomer , HttpStatus.CREATED);
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e);
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified customer: "+e);
		}
	}
	
	
	/**
	 * Tạo mới khách hàng theo user đã có
	 * @RequestParam username
	 * @RequestBody Customer pCustomer
	 */
	@CrossOrigin
	@PostMapping("/customer/create/user")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> createCustomerByUserId(@RequestParam("username") String username , @RequestBody Customer pCustomer ){
		try {
			Optional<User> userData = userRepository.findByUserName(username);
			Optional<Customer> existCustomer = pCustomerRepository.findCustomerByEmail(pCustomer.getEmail());
			if (userData.isPresent() &&  existCustomer.isEmpty()) {
				pCustomer.setUser(userData.get());
				Customer saveCustomer = pCustomerRepository.save(pCustomer);
				return new ResponseEntity<>(saveCustomer , HttpStatus.OK);
			} else if(userData.isPresent() && existCustomer.isPresent()) {
				User vUser = userData.get();
				Customer newCustomer = existCustomer.get();
				newCustomer.setFirstName(pCustomer.getFirstName());
				newCustomer.setLastName(pCustomer.getLastName());
				newCustomer.setAddress(pCustomer.getAddress());
				newCustomer.setEmail(pCustomer.getEmail());
				
				newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
				newCustomer.setCity(pCustomer.getCity());
				newCustomer.setCountry(pCustomer.getCountry());
				newCustomer.setPostalCode(pCustomer.getPostalCode());
				newCustomer.setUser(vUser);
				pCustomerRepository.save(newCustomer);
				return new ResponseEntity<>(newCustomer , HttpStatus.OK);
			}else {
				return new ResponseEntity<>( HttpStatus.NOT_FOUND);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("+++++++++:::::" + e.getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified customer: "+e.getCause().getMessage());
		}
	}
	
	/**
	 * Cập nhật khách hàng theo id
	 * @Param id
	 * @RequestBody Customer pCustomer
	 */
	@CrossOrigin
	@PutMapping("/customer/update/{id}")
	public ResponseEntity<Object> updateCustomerById(@PathVariable("id") int id , @RequestBody Customer pCustomer){
		try {
			Optional<Customer> customerData = pCustomerRepository.findById(id);
			if (customerData.isPresent()) {
				Customer newCustomer = customerData.get();
				newCustomer.setFirstName(pCustomer.getFirstName());
				newCustomer.setLastName(pCustomer.getLastName());
				newCustomer.setAddress(pCustomer.getAddress());
				newCustomer.setEmail(pCustomer.getEmail());
				newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
				newCustomer.setCity(pCustomer.getCity());
				newCustomer.setCountry(pCustomer.getCountry());
				newCustomer.setPostalCode(pCustomer.getPostalCode());
				Customer saveCustomer = pCustomerRepository.save(newCustomer);
				return new ResponseEntity<>(saveCustomer , HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified customer: " + id +"for update");
		}
	}
	
	/**
	 * Cập nhật khách hàng theo email
	 * @Param email
	 * @RequestBody Customer pCustomer
	 */
	@CrossOrigin
	@PutMapping("/customers/update/{email}")
	@PreAuthorize("hasRole('USER')")
	public ResponseEntity<Object> updateCustomerByEmail(@PathVariable("email") String email , @RequestBody Customer pCustomer){
		try {
			Optional<Customer> customerData = pCustomerRepository.findByEmail(email);
			if (customerData.isPresent()) {
				Customer newCustomer = customerData.get();
				newCustomer.setFirstName(pCustomer.getFirstName());
				newCustomer.setLastName(pCustomer.getLastName());
				newCustomer.setAddress(pCustomer.getAddress());
				newCustomer.setEmail(pCustomer.getEmail());
				newCustomer.setPhoneNumber(pCustomer.getPhoneNumber());
				newCustomer.setCity(pCustomer.getCity());
				newCustomer.setCountry(pCustomer.getCountry());
				newCustomer.setPostalCode(pCustomer.getPostalCode());
				Customer saveCustomer = pCustomerRepository.save(newCustomer);
				return new ResponseEntity<>(saveCustomer , HttpStatus.OK);
			}else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.badRequest().body("Failed to update specified customer: " + email +"for update");
		}
	}
	
	/**
	 * Xoá khách hàng theo id
	 * @Param id
	 */
	@CrossOrigin
	@DeleteMapping("/customer/delete/{id}")
	public ResponseEntity<Customer> deleteCustomerById(@PathVariable("id") int id){
		try {
			pCustomerRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
