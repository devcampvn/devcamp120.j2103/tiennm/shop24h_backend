package com.devcamp.shop24h;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiShop24HApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiShop24HApplication.class, args);
	}

}
